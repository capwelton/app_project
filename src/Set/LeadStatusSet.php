<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Project\Set;

include_once 'base.php';

/**
 * @method LeadStatus[] select()
 * @method LeadStatus   get()
 * @method LeadStatus   newRecord()
 * @method Func_App     App()
 * 
 * @property \ORM_StringField   $name
 */
class LeadStatusSet extends \app_TraceableRecordSet
{
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'LeadStatus');
        $this->setPrimaryKey('id');
        $this->setDescription('LeadStatus');
        
        $appC = $App->getComponentByName('LeadStatus');
        
        $this->addFields(
            ORM_StringField('name')->setDescription($appC->translate('Name'))
        );
        
        $this->addCustomFields();
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new LeadStatusBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new LeadStatusAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
    
    public function onUpdate()
    {
        $this->instanciateFirstStatuses();
    }
    
    private function instanciateFirstStatuses()
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Lead');
        
        $set = $App->LeadStatusSet();
        if ($set->select()->count() == 0) {
            $statusNames = array(
                $appC->translate('New'),
                $appC->translate('Transformed'),
                $appC->translate('Abandoned'),
                $appC->translate('Duplicate')
            );
            foreach ($statusNames as $statusName){
                $status = $set->newRecord();
                $status->name = $statusName;
                $status->save();
            }
        }
    }
    
    /**
     * Defines if records can be created by the current user.
     *
     * @return boolean
     */
    public function isCreatable()
    {
        return true;
    }
    
    /**
     * @return \ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }
    
    /**
     * @return \ORM_Criteria
     */
    public function isUpdatable()
    {
        return $this->all();
    }
    
    /**
     * @return \ORM_Criteria
     */
    public function isDeletable()
    {
        return $this->isUpdatable();
    }
}

class LeadStatusBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class LeadStatusAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}
