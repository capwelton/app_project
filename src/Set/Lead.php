<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Project\Set;

use Capwelton\App\Project\Set\LeadSet;

include_once 'base.php';

/**
 * @method LeadSet          getParentSet()
 * @method Func_App         App()
 * @method LeadType         type()
 * @method LeadStatus       status()
 * @method LeasSource       source()
 * 
 * @property string         $title
 * @property string         $content
 * @property date           $date
 * @property string         $origin
 * @property LeadType       $type()
 * @property LeadStatus     $status
 * @property LeasSource     $source
 * 
 */
class Lead extends \app_TraceableRecord
{
    const SUBFOLDER = 'leads';
    
    /**
     * @return bool
     */
    public function isReadable()
    {
        return true;
        $set = $this->getParentSet();
        return $set->select($set->isReadable()->is(true)->_AND_($set->id->is($this->id)))->count() == 1;
    }
    
    /**
     * @return bool
     */
    public function isUpdatable()
    {
        return true;
        $set = $this->getParentSet();
        return $set->select($set->isReadable()->is(true)->_AND_($set->id->is($this->id)))->count() == 1;
    }
    
    /**
     * @return bool
     */
    public function isDeletable()
    {
        return true;
        $set = $this->getParentSet();
        return $set->select($set->isReadable()->is(true)->_AND_($set->id->is($this->id)))->count() == 1;
    }
    
    /**
     * Get the upload path for files related to this project
     * @return \bab_Path
     */
    public function uploadPath()
    {
        if (! isset($this->id)) {
            return null;
        }
        
        require_once $GLOBALS['babInstallPath'] . 'utilit/path.class.php';
        
        $path = $this->App()->getUploadPath();
        $path->push(self::SUBFOLDER);
        $path->push($this->id);
        return $path;
    }
}
