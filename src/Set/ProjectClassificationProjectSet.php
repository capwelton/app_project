<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Project\Set;

include_once 'base.php';

/**
 * @method ProjectClassificationProject[]   select()
 * @method ProjectClassificationProject     get()
 * @method ProjectClassificationProject     newRecord()
 * @method Func_App                         App()
 * @method ProjectClassificationSet         projectClassification()
 * @method ProjectSet                       project()
 * 
 * @property ProjectClassificationSet       $projectClassification
 * @property ProjectSet                     $project
 */
class ProjectClassificationProjectSet extends \app_TraceableRecordSet
{
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'ProjectClassificationProject');
        $this->setPrimaryKey('id');
        $this->setDescription('ProjectClassificationProject');
        
        $this->hasOne('projectClassification', $App->ProjectClassificationSetClassName());
        $this->hasOne('project', $App->ProjectSetClassName());
        
        $this->addCustomFields();
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new ProjectClassificationProjectBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new ProjectClassificationProjectAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
    
    /**
     * @param int	$project		The project id
     *
     * @return \ORM_Iterator
     */
    public function selectForProject($project)
    {
        return $this->select($this->project->is($project));
    }
}

class ProjectClassificationProjectBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class ProjectClassificationProjectAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}