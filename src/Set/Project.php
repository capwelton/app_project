<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Project\Set;

use Capwelton\App\Project\Set\ProjectSet;
use Capwelton\App\SalesDocuments\Set\DeliveryOrderSet;
use Capwelton\App\SalesDocuments\Set\Document;
use Capwelton\App\SalesDocuments\Set\CreditNoteSet;
use Capwelton\App\SalesDocuments\Set\InvoiceDraftSet;
use Capwelton\App\SalesDocuments\Set\InvoiceSet;
use Capwelton\App\SalesDocuments\Set\OrderSet;
use Capwelton\App\SalesDocuments\Set\QuotationSet;

include_once 'base.php';

/**
 * @method ProjectSet       getParentSet()
 * @method \Func_App        App()
 * @method Lead             lead()
 * @method Organization     customer()
 * @method ProjectStatus    status()
 * @method Contact          responsible()
 * @method Address          address()
 * @method Contact          referralContact()
 * @method Project          parentProject()
 * @method Organization     responsibleOrganization()
 * 
 * @property string         $name
 * @property string         $description
 * @property string         $startingDate
 * @property string         $number
 * @property float          $amount
 * @property bool           $amountIsTaxInclusive
 * @property string         $milestoneFrequency         Only if the optional component SALESDOCUMENT is installed
 * @property string         $orderFrequency             Only if the optional component SALESDOCUMENT is installed
 * @property float          $milestoneMaxAmount         Only if the optional component SALESDOCUMENT is installed
 * @property date           $orderCreationCycleStart    Only if the optional component SALESDOCUMENT is installed
 * @property Lead           $lead
 * @property Organization   $customer
 * @property ProjectStatus  $status
 * @property Contact        $responsible
 * @property Address        $address
 * @property Contact        $referralContact
 * @property Project        $parentProject
 * @property Organization   $responsibleOrganization
 */
class Project extends \app_TraceableRecord
{
    const SUBFOLDER = 'projects';
    const PHOTOSUBFOLDER = 'photo';
    
    /**
     * @return bool
     */
    public function isReadable()
    {
        return true;
        $set = $this->getParentSet();
        return $set->select($set->isReadable()->is(true)->_AND_($set->id->is($this->id)))->count() == 1;
    }
    
    /**
     * @return bool
     */
    public function isUpdatable()
    {
        return true;
        $set = $this->getParentSet();
        return $set->select($set->isReadable()->is(true)->_AND_($set->id->is($this->id)))->count() == 1;
    }
    
    /**
     * @return bool
     */
    public function isDeletable()
    {
        return true;
        $set = $this->getParentSet();
        return $set->select($set->isReadable()->is(true)->_AND_($set->id->is($this->id)))->count() == 1;
    }
    
    /**
     * Returns the formatted number for the project.
     * @return string
     */
    public function getFullNumber()
    {
        return $this->getYear() . str_pad($this->number, 4, '0', STR_PAD_LEFT);
    }
    
    public function getYear()
    {
        return substr($this->createdOn, 0, 4);
    }
    
    /**
     * Initialize a unique number for the project.
     * @return Project
     */
    public function initFullNumber()
    {
        if (empty($this->number)) {
            $this->startingDate = date('Y-m-d');
            $projectCounterSet = $this->App()->ProjectCounterSet();
            $this->number = $projectCounterSet->newNumber($this->getYear());
        }
        return $this;
    }
    
    public function save($notrace = false)
    {
        $this->initFullNumber();
        return parent::save($notrace);
    }
    
    /**
     * Get the upload path for files related to this project
     * @return \bab_Path
     */
    public function uploadPath()
    {
        if (! isset($this->id)) {
            return null;
        }
        
        require_once $GLOBALS['babInstallPath'] . 'utilit/path.class.php';
        
        $path = $this->App()->getUploadPath();
        $path->push(self::SUBFOLDER);
        $path->push($this->id);
        return $path;
    }
    
    /**
     * Get the upload path for the photo file
     * this method can be used with the image picker widget
     *
     * @return \bab_Path
     */
    public function getPhotoUploadPath()
    {
        if (!isset($this->id)) {
            return null;
        }
        $path = $this->uploadPath();
        $path->push(self::PHOTOSUBFOLDER);
        return $path;
    }
    
    /**
     * Return the full path of the photo file
     * The photo field contain a file name or a description of the photo
     * this method return null if the is no photo to display
     *
     * @return \bab_Path | null
     */
    public function getPhotoPath()
    {
        $uploadpath = $this->getPhotoUploadPath();
        
        if (!isset($uploadpath)) {
            return null;
        }
        
        $W = bab_Widgets();
        
        $uploaded = $W->ImagePicker()->getFolderFiles($uploadpath);
        
        if (!isset($uploaded)) {
            return null;
        }
        
        foreach($uploaded as $file) {
            return $file->getFilePath();
        }
        
        return null;
    }
    
    /**
     * Move photo to photo upload path
     * this method is used to import a photo from a temporary directory of the filePicker widget or another file
     * warning, the default behavior remove the source file
     *
     * @param	\bab_Path 	$sourcefile
     * @param	bool		$temporary			default is true, if the sourcefile is a filePicker temporary file
     * @return 	bool
     */
    public function importPhoto(\bab_Path $sourcefile, $temporary = true)
    {
        $uploadpath = $this->getPhotoUploadPath();
        
        if (!isset($uploadpath)) {
            return false;
        }
        
        $uploadpath->createDir();
        
        if (!$temporary) {
            $W = bab_Widgets();
            return $W->imagePicker()->setFolder($uploadpath)->importFile($sourcefile);
        }
        
        $original = $sourcefile->toString();
        $uploadpath->push(basename($original));
        
        return rename($original, $uploadpath->toString());
    }
    
    /**
     * Return the status as a human-readable string.
     *
     * @return string
     */
    public function getStatusName()
    {
        $this->App()->ProjectStatusSet();
        if ($status = $this->status()) {
            return $status->name;
        }
        return '';
    }
    
    /**
     * Updates the status of the project and keep track of the status updates history.
     *
     * @param	int		$status				The new status id.
     * @param	string	$comment			A comment to associate to the status change.
     * @param	string	$newStatusDateTime	The iso-formatted datetime of the status change.
     *
     * @return Project
     */
    public function updateStatus($status, $comment = null, $newStatusDateTime = null)
    {
        $App = $this->App();
        
        $projectStatusHistorySet = $App->ProjectStatusHistorySet();
        $projectStatusHistory = $projectStatusHistorySet->newRecord();
        $projectStatusHistory->project = $this->id;
        $projectStatusHistory->projectStatus = $status;
        if (!isset($newStatusDateTime) || empty($newStatusDateTime) || $newStatusDateTime === '0000-00-00 00:00:00') {
            $newStatusDateTime = (new \DateTime('now'))->format('Y-m-d');
        }
        $projectStatusHistory->date = $newStatusDateTime;
        $projectStatusHistory->comment = $comment;
        
        $latestStatuses = $projectStatusHistorySet->select(
            $projectStatusHistorySet->project->is($this->id)
        )->orderDesc($projectStatusHistorySet->date)
        ->orderDesc($projectStatusHistorySet->id);
        
        foreach ($latestStatuses as $latestStatus) {
            break;
        }
        $projectStatusHistory->save();
        
        $cleanSet = $App->ProjectSet();
        $project = $cleanSet->request($cleanSet->id->is($this->id));
        
        if (isset($latestStatus)) {
            // Check if the new status becomes the new latest status.
            if ($projectStatusHistory->date >= $latestStatus->date) {
                $project->status = $projectStatusHistory->projectStatus;
                
                // If the new latest status is the same as the previous latest status, we remove the previous one.
                if ($projectStatusHistory->projectStatus == $latestStatus->status) {
                    $projectStatusHistorySet->delete($projectStatusHistorySet->id->is($latestStatus->id));
                }
            } else {
                $project->status = $latestStatus->projectStatus;
            }
        } else {
            $project->status = $projectStatusHistory->projectStatus;
        }
        
        $project->save();
        
        return $this;
    }
    
    /**
     * Updates the status of the project and keep track of the status updates history.
     *
     * @param	string	$key				The new status reference id.
     * @param	string	$comment			A comment to associate to the status change.
     * @param	string	$newStatusDateTime	The iso-formatted datetime of the status change.
     *
     * @return Project
     */
    public function updateStatusByReference($key, $comment = null, $newStatusDateTime = null)
    {
        $statusSet = $this->App()->ProjectStatusSet();
        $status = $statusSet->get($statusSet->reference->is($key));
        if (!isset($status)) {
            return $this;
        }
        
        $result = $this->updateStatus($status->id, $comment, $newStatusDateTime);
        
        return $result;
    }
    
    /**
     * Updates the project status with the latest in the deal status history.
     */
    public function updateLatestStatus()
    {
        $App = $this->App();
        $projectStatusHistorySet = $App->ProjectStatusHistorySet();
        
        $projectSet = $App->ProjectSet();
        $project = $projectSet->get($projectSet->id->is($this->id)); //Remove links
        
        $latestStatuses = $projectStatusHistorySet->select(
            $projectStatusHistorySet->project->is($project->id)
        )->orderDesc($projectStatusHistorySet->date)
        ->orderDesc($projectStatusHistorySet->id);
        
        $project->status = 0;
        foreach ($latestStatuses as $latestStatus) {
            $project->status = $latestStatus->projectStatus;
            break;
        }
        $project->save();
    }
    
    /**
     * Return the latest given status history object
     * @return ProjectStatusHistory|NULL
     */
    public function getLatestGivenStatusHistory()
    {
        $App = $this->App();
        $projectStatusHistorySet = $App->ProjectStatusHistorySet();
        
        $latestStatuses = $projectStatusHistorySet->select(
            $projectStatusHistorySet->project->is($this->id)
        )->orderDesc($projectStatusHistorySet->date)
        ->orderDesc($projectStatusHistorySet->id);
        
        foreach ($latestStatuses as $latestStatus) {
            return $latestStatus;
        }
        
        return null;
    }
    
    /**
     * Returns the output values of the record as an associative array suited for a form.
     * Joined records are returned as sub-arrays.
     * @see \ORM_Record::getFormOutputValues()
     * @return array
     */
    public function getFormOutputValues()
    {
        $values = parent::getFormOutputValues();
        if($address = $this->address()){
            $values['address'] = $address->getFormOutputValues();
        }
        return $values;
    }

    /**
     * Returns wether or not project contains asked for status in its history
     * @param	int		$statusId		Searched for status id.
     * @return bool
     */
    public function hasLoggedStatus($statusId)
    {
        $App = $this->App();
        $projectStatusHistorySet = $App->ProjectStatusHistorySet();

        return $projectStatusHistorySet->select($projectStatusHistorySet->all(
            $projectStatusHistorySet->projectStatus->is($statusId),
            $projectStatusHistorySet->project->is($this->id)
        ))->count() > 0;
    }

    /**
     * Returns last transformed sales document for this project
     * @return Document|NULL
     */
    public function getLatestDocument()
    {
        $App = $this->App();
        
        $salesC = $App->getComponentByName('Quotation');
        if(!$salesC){
            return null;
        }
        
        /* @var $set CreditNoteSet */
        $set = $App->CreditNoteSet();
        $records = $set->select($set->project->is($this->id))->orderDesc($set->createdOn);
        foreach ($records as $record){
            return $record;
        }
        
        /* @var $set InvoiceSet */
        $set = $App->InvoiceSet();
        $records = $set->select($set->project->is($this->id))->orderDesc($set->createdOn);
        foreach ($records as $record){
            return $record;
        }
        
        /* @var $set InvoiceDraftSet */
        $set = $App->InvoiceDraftSet();
        $records = $set->select($set->project->is($this->id))->orderDesc($set->createdOn);
        foreach ($records as $record){
            return $record;
        }
        
        /* @var $set DeliveryOrderSet */
        $set = $App->DeliveryOrderSet();
        $records = $set->select($set->project->is($this->id))->orderDesc($set->createdOn);
        foreach ($records as $record){
            return $record;
        }
        
        /* @var $set OrderSet */
        $set = $App->OrderSet();
        $records = $set->select($set->project->is($this->id))->orderDesc($set->createdOn);
        foreach ($records as $record){
            return $record;
        }
        
        /* @var $set QuotationSet */
        $set = $App->QuotationSet();
        $records = $set->select($set->project->is($this->id))->orderDesc($set->createdOn);
        foreach ($records as $record){
            return $record;
        }
        
        return null;
    }


}
