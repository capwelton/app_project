<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Project\Set;

include_once 'base.php';

/**
 * @method ProjectStatusSet getParentSet()
 * @method ProjectStatus    lf()
 * @method ProjectStatus    lr()
 * @method ProjectStatus    parent()
 * @method Func_App         App()
 *
 * @property string         $name
 * @property string         $reference
 * @property string         $color
 * @property string         $icon
 * @property ProjectStatus  $lf
 * @property ProjectStatus  $lr
 * @property ProjectStatus  $parent
 */
class ProjectStatus extends \app_TraceableRecord
{
    /**
     * Returns an iterator on the status's descendants.
     *
     * @return self[]
     */
    public function getDescendants()
    {
        $set = $this->getParentSet();
        return $set->select($set->isDescendantOf($this))->orderAsc($set->lf);
    }
    
    /**
     * Returns an array of the status's ascendants.
     *
     * @return self[]
     */
    public function getAscendants()
    {
        $set = $this->getParentSet();
        
        $ascendants = array();
        $parentId = $this->parent;
        while ($parentId != 1 && $parent = $set->get($parentId)) {
            $ascendants[$parentId] = $parent;
            $parentId = $parent->parent;
        }
        return $ascendants;
    }
    
    /**
     * Returns an iterator on the status's children.
     *
     * @return self[]
     */
    public function getChildren()
    {
        $set = $this->getParentSet();
        return $set->select($set->isChildOf($this))->orderAsc($set->lf);
    }
    
    /**
     *
     *
     * @return string[]
     */
    public function getPathname()
    {
        $pathname = array();
        $ascendants = $this->getAscendants();
        foreach ($ascendants as $status) {
            $pathname[] = $status->name;
        }
        $pathname[] = $this->name;
        return $pathname;
    }
    
    /**
     * Returns the first child of the node or null.
     *
     * @return self|null
     */
    public function getFirstChild()
    {
        $set = $this->getParentSet();
        $children = $set->select($set->parent->is($this->id));
        $children->orderAsc($set->lf);
        foreach ($children as $child) {
            return $child;
        }
        return null;
    }
    
    /**
     * Returns the last child of the node or null.
     *
     * @return self|null
     */
    public function getLastChild()
    {
        $set = $this->getParentSet();
        $children = $set->select($set->parent->is($this->id));
        $children->orderDesc($set->lf);
        foreach ($children as $child) {
            return $child;
        }
        return null;
    }
    
    /**
     * Saves the specified status as the last child.
     *
     * @return bool
     */
    public function appendChild(ProjectStatus $status)
    {
        $status->parent = $this->id;
        if ($lastChild = $this->getLastChild()) {
            $lr = $lastChild->lr;
        } else {
            $lr = $this->lf;
        }
        $this->getParentSet()->update($lr);
        $status->lf = $lr + 1;
        $status->lr = $lr + 2;
        
        return $status->save();
    }
}