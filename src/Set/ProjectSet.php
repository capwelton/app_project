<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Project\Set;

use Capwelton\App\Project\Set\RecordViews\ProjectDefaultRecordView;
use Capwelton\App\Project\Set\RecordViews\ProjectPreviewRecordView;

include_once 'base.php';

/**
 * @method Project[]        select()
 * @method Project          get()
 * @method Project          newRecord()
 * @method Func_App         App()
 * @method LeadSet          lead()
 * @method OrganizationSet  customer()
 * @method ProjectStatusSet status()
 * @method ContactSet       responsible()
 * @method AddressSet       address()
 * @method ContactSet       referralContact()
 * @method ProjectSet       parentProject()
 * @method OrganizationSet  responsibleOrganization()
 * 
 * @property \ORM_StringField   $name
 * @property \ORM_TextField     $description
 * @property \ORM_DateField     $startingDate
 * @property \ORM_StringField   $number
 * @property \ORM_CurrencyField $amount
 * @property \ORM_BoolField     $amountIsTaxInclusive
 * @property \ORM_RruleField    $milestoneFrequency         Only if the optional component SALESDOCUMENT is installed
 * @property \ORM_RruleField    $orderFrequency             Only if the optional component SALESDOCUMENT is installed
 * @property \ORM_DecimalField  $milestoneMaxAmount         Only if the optional component SALESDOCUMENT is installed
 * @property \ORM_DateField     $orderCreationCycleStart    Only if the optional component SALESDOCUMENT is installed
 * @property LeadSet            $lead
 * @property OrganizationSet    $customer
 * @property ProjectStatusSet   $status
 * @property ContactSet         $responsible
 * @property AddressSet         $address
 * @property ContactSet         $referralContact
 * @property ProjectSet         $parentProject
 * @property OrganizationSet    $responsibleOrganization
 */
class ProjectSet extends \app_TraceableRecordSet
{
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'Project');
        $this->setPrimaryKey('id');
        $this->setDescription('Project');
        $appC = $App->getComponentByName('Project');
        
        $this->addFields(
            ORM_StringField('name')->setDescription($appC->translate('Name')),
            ORM_TextField('description')->setDescription($appC->translate('Description')),
            ORM_DateField('startingDate')->setDescription($appC->translate('Starting date for project')),
            ORM_StringField('number')->setDescription($appC->translate('Number')),
            ORM_CurrencyField('amount')->setDescription($appC->translate('Amount')),
            ORM_BoolField('amountIsTaxInclusive')->setDescription($appC->translate('Amount is tax inclusive'))->setOutputOptions($appC->translate('No'), $appC->translate('Yes'))
        );
        
        if($salesC = $App->getComponentByName('SALESDOCUMENT')){
            $this->addFieldKey(
                ORM_RruleField('milestoneFrequency')->setDescription($salesC->translate('Generate invoice milestones on order creation')),
                ORM_RruleField('orderFrequency')->setDescription($salesC->translate('Generate order from project')),
                ORM_DecimalField('milestoneMaxAmount')->setDescription($salesC->translate('Auto recurring milestones are created up to this amount')),
                ORM_DateField('orderCreationCycleStart')->setDescription($salesC->translate('Orders are created from this date if orderFrequency is set'))
            );
        }
        
//         $this->hasOne('lead', $App->LeadSetClassName());
        $this->hasOne('customer', $App->OrganizationSetClassName());
        $this->hasOne('status', $App->ProjectStatusSetClassName());
        $this->hasOne('responsible', $App->ContactSetClassName());
        $this->hasOne('address', $App->AddressSetClassName());
        $this->hasOne('referralContact', $App->ContactSetClassName()); // Contact referent chez le client
        $this->hasOne('parentProject', $App->ProjectSetClassName()); // Parent project if this is an additional project.
        $this->hasOne('responsibleOrganization', $App->OrganizationSetClassName()); // Organisme responsable
        
        $this->addCustomFields();
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new ProjectBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new ProjectAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
    
    public function getRequiredComponents()
    {
        return array(
            'ADDRESS',
            'CONTACT',
            'ORGANIZATION',
            'ATTACHMENT'
        );
    }
    
    public function getOptionalComponents()
    {
        return array(
            'SALESDOCUMENT'  
        );
    }
    
    public function onUpdate()
    {
        $defaultView = new ProjectDefaultRecordView($this->App());
        $defaultView->generate();
        $previewView = new ProjectPreviewRecordView($this->App());
        $previewView->generate();
    }
    
    /**
     *
     * @return self
     */
    public function addFullNumberField()
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        $this->addFields(
            $this->createdOn->left(4)->concat($this->number->leftPad(4, '0'))
            ->setName('fullNumber')
            ->setDescription($appC->translate('Number'))
        );
        return $this;
    }
    
    /**
     * Defines if records can be created by the current user.
     *
     * @return boolean
     */
    public function isCreatable()
    {
        return true;
    }
    
    
    /**
     * Returns a criterion matching records readable by the current user.
     *
     * @since 1.0.21
     *
     * @return \ORM_Criterion
     */
    public function isReadable()
    {
        return $this->all();
    }
    
    
    /**
     * Returns a criterion matching records updatable by the current user.
     *
     * @since 1.0.21
     *
     * @return \ORM_Criterion
     */
    public function isUpdatable()
    {
        return $this->all();
    }
    
    /**
     * Returns a criterion matching records deletable by the current user.
     *
     * @since 1.0.21
     *
     * @return \ORM_Criterion
     */
    public function isDeletable()
    {
        return $this->all();
    }
    
    public function getAscendantKeys()
    {
        return array(
            'customer'
        );
    }
    
    /**
     * @return \ORM_Criteria
     */
    public function hasImpliedClassification($classification)
    {
        return $this->hasClassification($classification);
    }
    
    
    
    /**
     * @return \ORM_Criteria
     */
    public function hasClassification($classification)
    {
        if (empty($classification)) {
            return $this->all();
        }
        $Crm = $this->Crm();
        $projectClassificationSet = $Crm->ProjectClassificationSet();
        $projectClassificationSet->classification();
        
        return $this->id->in($projectClassificationSet->classification->name->is($classification), 'project');
    }
    
    
    
    /**
     * @param int|int[]     $classification ids.
     * @return \ORM_Criteria
     */
    public function hasClassificationId($classifications)
    {
        if (empty($classifications)) {
            return $this->all();
        }
        $Crm = $this->Crm();
        
        $projectClassificationSet = $Crm->ProjectClassificationSet();
        
        return $this->id->in($projectClassificationSet->classification->in($classifications), 'project');
    }
    
    
    /**
     * @param int   $status
     * @return \ORM_Criteria
     */
    public function hasStatus($status)
    {
        $statusSet = $this->Crm()->ProjectStatusSet();
        $status = $statusSet->get($statusSet->id->is($status));
        
        return $this->status->in($statusSet->isDescendantOf($status)->_OR_($statusSet->id->is($status->id)), 'id');
    }
}

class ProjectBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class ProjectAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}