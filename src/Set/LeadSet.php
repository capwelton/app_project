<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Project\Set;

use Capwelton\App\Project\Set\RecordViews\LeadDefaultRecordView;
use Capwelton\App\Project\Set\RecordViews\LeadPreviewRecordView;

include_once 'base.php';

/**
 * @method Lead[]           select()
 * @method Lead             get()
 * @method Lead             newRecord()
 * @method Func_App         App()
 * @method LeadTypeSet      type()
 * @method LeadStatusSet    status()
 * @method LeadSourceSet    source()         
 * 
 * @property \ORM_StringField   $title
 * @property \ORM_TextField     $content
 * @property \ORM_DateTimeField $date
 * @property \ORM_StringField   $origin
 * @property LeadTypeSet        $type
 * @property LeadStatusSet      $status
 * @property LeadSourceSet      $source
 */
class LeadSet extends \app_TraceableRecordSet
{
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'Lead');
        $this->setPrimaryKey('id');
        $this->setDescription('Lead');
        
        $appC = $App->getComponentByName('Lead');
        
        $this->addFields(
            ORM_StringField('title')->setDescription($appC->translate('Title')),
            ORM_TextField('content')->setDescription($appC->translate('Content')),
            ORM_DateTimeField('date')->setDescription($appC->translate('Date')),
            ORM_StringField('origin')->setDescription($appC->translate('Origin'))
        );
        
        $this->hasOne('type', $App->LeadTypeSetClassName());
        $this->hasOne('status', $App->LeadStatusSetClassName());
        $this->hasOne('source', $App->LeadSourceSetClassName());
        
        $this->addCustomFields();
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new LeadBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new LeadAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
    
    public function getRequiredComponents()
    {
        return array();
    }
    
    public function getOptionalComponents()
    {
        return array();
    }
    
    public function onUpdate()
    {
        $defaultView = new LeadDefaultRecordView($this->App());
        $defaultView->generate();
        $previewView = new LeadPreviewRecordView($this->App());
        $previewView->generate();
    }
    
    /**
     * Defines if records can be created by the current user.
     *
     * @return boolean
     */
    public function isCreatable()
    {
        return true;
    }
    
    
    /**
     * Returns a criterion matching records readable by the current user.
     *
     * @since 1.0.21
     *
     * @return \ORM_Criterion
     */
    public function isReadable()
    {
        return $this->all();
    }
    
    
    /**
     * Returns a criterion matching records updatable by the current user.
     *
     * @since 1.0.21
     *
     * @return \ORM_Criterion
     */
    public function isUpdatable()
    {
        return $this->all();
    }
    
    /**
     * Returns a criterion matching records deletable by the current user.
     *
     * @since 1.0.21
     *
     * @return \ORM_Criterion
     */
    public function isDeletable()
    {
        return $this->all();
    }
}

class LeadBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class LeadAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}
