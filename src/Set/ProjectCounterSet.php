<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Project\Set;

use Capwelton\App\ContactOrganization\Set\ContactSet;
use Capwelton\App\Address\Set\AddressSet;
use Capwelton\App\ContactOrganization\Set\OrganizationSet;

include_once 'base.php';

/**
 * @method ProjectCounter[] select()
 * @method ProjectCounter   get()
 * @method ProjectCounter   newRecord()
 * @method Func_App         App()
 * 
 * @property \ORM_IntField  $year
 * @property \ORM_IntField  $number
 */
class ProjectCounterSet extends \app_TraceableRecordSet
{
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'ProjectCounter');
        $this->setPrimaryKey('id');
        $this->setDescription('ProjectCounter');
        $appC = $App->getComponentByName('ProjectCounter');
        
        $this->addFields(
            ORM_IntField('year')->setDescription($appC->translate('Year')),
            ORM_IntField('number')->setDescription($appC->translate('Number of the next project for this year'))
        );
        
        $this->addCustomFields();
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new ProjectCounterBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new ProjectCounterAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
    
    /**
     * Returns a new project number for the specified year.
     *
     * @param int	$year     The year.
     * @return int
     */
    public function newNumber($year)
    {
        $counter = $this->get($this->year->is($year));
        if (!isset($counter)) {
            $counter = $this->newRecord();
            $counter->year = $year;
            $counter->number = 1;
        }
        $number = $counter->number;
        $counter->number++;
        $counter->save();
        return $number;
    }
}

class ProjectCounterBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class ProjectCounterAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}