<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Project\Set\RecordViews;

class ProjectPreviewRecordView
{
    /**
     * @var \Func_App
     */
    private $app;
    public function __construct(\Func_App $App)
    {
        $this->app = $App;
    }
    
    public function generate($force = false)
    {
        $App = $this->app;
        $customSectionSet = $App->CustomSectionSet();
        $object = $App->classPrefix.'Project';
        $view = 'preview';
        
        $sectionCriteria = $customSectionSet->all(
            $customSectionSet->object->is($object)
            ->_AND_($customSectionSet->view->is($view))
            );
        
        $customSections = $customSectionSet->select(
            $sectionCriteria
            );
        
        if($customSections->count() > 0 && !$force)
        {
            return $this;
        }
        
        $customContainerSet = $App->CustomContainerSet();
        
        $containerCriteria = $customContainerSet->all(
            $customContainerSet->object->is($object)
            ->_AND_($customContainerSet->view->is($view))
        );
        
        $customContainerSet->delete($containerCriteria);
        $customSectionSet->delete($sectionCriteria);
        
        $rank = 0;
        
        $container = $customContainerSet->newRecord();
        $container->view = $view;
        $container->object = $object;
        $container->sizePolicy = 'col-md-12';
        $container->layout = 'vbox';
        $container->save();
        
        //Project
        $customSection = $customSectionSet->newRecord();
        $customSection->sizePolicy = 'col-md-12';
        $customSection->fieldsLayout = 'wideHorizontalLabel';
        $customSection->object = $object;
        $customSection->fields = 'name:type=title1;label=__;classname=title1 colored;sizePolicy=,fullNumber:type=title2;label=__;classname=title2;sizePolicy=widget-strong colored,createdOn,customer,status,classifications,description,amount,address';
        $customSection->classname = 'box red';
        $customSection->editable = true;
        $customSection->name = '';
        $customSection->rank = $rank;
        $customSection->view = $view;
        $customSection->container = $container->id;
        $customSection->save();
    }
}