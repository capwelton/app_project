<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Project\Set;

include_once 'base.php';

/**
 * @method ProjectStatusHistory[]   select()
 * @method ProjectStatusHistory     get()
 * @method ProjectStatusHistory     newRecord()
 * @method ProjectSet               project()
 * @method ProjectStatusSet         projectStatus()
 * @method Func_App                 App()
 * 
 * @property \ORM_DateField         $date
 * @property \ORM_TextField         $comment
 * @property ProjectSet             $project
 * @property ProjectStatusSet       $projectStatus
 */
class ProjectStatusHistorySet extends \app_TraceableRecordSet
{
    public function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->setTableName($App->classPrefix.'ProjectStatusHistory');
        $this->setPrimaryKey('id');
        $this->setDescription('ProjectStatusHistory');
        $appC = $App->getComponentByName('ProjectStatusHistory');
        
        $this->addFields(
            ORM_DateField('date')->setDescription($appC->translate('Status date')),
            ORM_TextField('comment')->setDescription($appC->translate('Comment'))
        );
        
        $this->hasOne('project', $App->ProjectSetClassName());
        $this->hasOne('projectStatus', $App->ProjectStatusSetClassName());
        
        $this->addCustomFields();
    }
    
    /**
     *
     * {@inheritdoc}
     * @see \app_TraceableRecordSet::save()
     */
    public function save(\ORM_Record $record, $noTrace = false)
    {
        $event = new ProjectStatusHistoryBeforeSaveEvent($record);
        bab_fireEvent($event);
        
        $result = parent::save($record);
        
        $event = new ProjectStatusHistoryAfterSaveEvent($record);
        bab_fireEvent($event);
        
        return $result;
    }
    
    /**
     * @return \ORM_Criteria
     */
    public function isReadable()
    {
        return $this->all();
    }
}

class ProjectStatusHistoryBeforeSaveEvent extends \RecordBeforeSaveEvent
{
    
}

class ProjectStatusHistoryAfterSaveEvent extends \RecordAfterSaveEvent
{
    
}