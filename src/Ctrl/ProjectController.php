<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Project\Ctrl;

use Capwelton\App\Project\Set\Project;
use Capwelton\App\SalesDocuments\Set\Document;
use Capwelton\App\SalesDocuments\Set\QuotationSet;
use Capwelton\App\SalesDocuments\Set\Quotation;

$App = app_App();
$App->includeRecordController();
$App->setCurrentComponentByName('Project');

/**
 * This controller manages actions that can be performed on projects.
 *
 * @method \Func_App    App()
 */
class ProjectController extends \app_ComponentCtrlRecord
{   
    /**
    * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('Project'));
    }
    
    /**
     * {@inheritDoc}
     * @see \app_CtrlRecord::getAvailableDisplayFields()
     */
    public function getAvailableDisplayFields()
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        $availableFields = array(
            'photo' => array(
                'name' => 'photo',
                'description' => $appC->translate('Photo')
            ),
            'address' => array(
                'name' => 'address',
                'description' => $appC->translate('Address')
            ),
            'classifications' => array(
                'name' => 'classifications',
                'description' => $appC->translate('Classifications')
            ),
            'timeline' => array(
                'name' => 'timeline',
                'description' => $appC->translate('Timeline')
            ),
            'responsible' => array(
                'name' => 'responsible',
                'description' => $appC->translate('Project responsible')
            ),
            'startingDate' => array(
                'name' => 'startingDate',
                'description' => $appC->translate('Starting date')
            )
        );
        
        if($attachmentC = $App->getComponentByName('ATTACHMENT')){
            $availableFields['attachments'] = array(
                'name' => 'attachments',
                'description' => $attachmentC->translate('Attached files')
            );
        }
        if($tagC = $App->getComponentByName('TAG')){
            $availableFields['tags'] =  array(
                'name' => 'tags',
                'description' => $tagC->translate('Tags')
            );
        }
        if($noteC = $App->getComponentByName('NOTE')){
            $availableFields['notes'] = array(
                'name' => 'notes',
                'description' => $noteC->translate('Notes')
            );
        }
        if($teamC = $App->getComponentByName('TEAM')){
            $availableFields['teams'] = array(
                'name' => 'teams',
                'description' => $teamC->translate('Teams')
            );
        }
        if($taskC = $App->getComponentByName('TASK')){
            $availableFields['pendingTasks'] = array(
                'name' => 'pendingTasks',
                'description' => $taskC->translate('Pending tasks')
            );
            $availableFields['completedTasks'] = array(
                'name' => 'completedTasks',
                'description' => $taskC->translate('Completed tasks')
            );
        }
        if($quotationC = $App->getComponentByName('QUOTATION')){
            $availableFields['quotations'] = array(
                'name' => 'quotations',
                'description' => $quotationC->translate('Quotations')
            );
        }

        $availableFields = array_merge($availableFields, parent::getAvailableDisplayFields());

        return $availableFields;
    }
    /**
     * @param Project $record
     * @return array
     */
    protected function getActions(Project $record, $currentView = '')
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        $W = bab_Widgets();
        
        $ctrl = $this->proxy();
        $actions = array();
        $moreActions = array();
        $documentsActions = array();


        if ($record->isUpdatable()) {
            $action = $ctrl->edit($record->id);
            $action->setTitle($appC->translate('Edit'));
            $action->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT);
            $actions['edit'] = $action;
        }

        if ($record->isReadable()) {
            $action = $App->Controller()->Attachment()->attachments($record->getRef());
            $action->setTitle($App->translate('Attached files'));
            $action->setIcon(\Func_Icons::APPS_FILE_MANAGER);
            $documentsActions['attachedFiles'] = $action;
        }


        if ($record->isDeletable()) {
            $action = $ctrl->confirmDelete($record->id);
            $action->setTitle($appC->translate('Delete'));
            $action->setIcon(\Func_Icons::ACTIONS_EDIT_DELETE);
            $moreActions['delete'] = $action;
        }

        if (bab_isUserAdministrator()) {
            $customContainerCtrl = $App->Controller()->CustomContainer();
            $views = $record->getViews();
            $className = $record->getClassName();

            foreach ($views as $view) {
                $action = $W->Link(
                    sprintf($App->translate('Edit view layout %s'), $view),
                    $customContainerCtrl->editContainers($className, $view)
                )->setIcon(\Func_Icons::ACTIONS_VIEW_PIM_JOURNAL)->addClass('dropdown-item');
                
                if($view == $currentView){
                    $action->addClass('active-view');
                }
                
                $moreActions['editSections_' . $view] = $action;
            }
            
            $action = $customContainerCtrl->importContainersConfirm($className, $view);
            $action->setTitle($App->translate('Import view layout'));
            $action->setIcon(\Func_Icons::ACTIONS_VIEW_PIM_JOURNAL);
            $moreActions['importSectionsConfirm'] = $action;
        }

        $actions['documents'] = array(
            'icon' => \Func_Icons::APPS_FILE_MANAGER,
            'title' => $appC->translate('Documents'),
            'items' => array(
                $documentsActions
            )
        );
        $actions['more'] = array(
            'icon' => 'actions-context-menu'/*Func_Icons::ACTIONS_CONTEXT_MENU*/,
            'title' => $appC->translate('More'),
            'items' => array(
                $moreActions
            )
        );
        return $actions;
    }

    /**
     * Returns a page with the record information.
     *
     * @param int	$id
     * @return \app_Page
     */
    public function display($id, $view = '', $itemId = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Project');

        $recordSet = $this->getRecordSet();

        $record = $recordSet->request($id);
        if (!$record->isReadable()) {
            throw new \app_AccessException($appC->translate('You do not have access to this page'));
        }

        $W = bab_Widgets();
    
        $Ui = $App->Ui();
        $fullFrame = $Ui->ProjectFullFrame($record);
        $fullFrame->setView($view);

        $actions = $this->getActions($record, $view);

        $mainMenu = $W->Html($this->createMenu($actions));
        $mainMenu->addClass('app-record-nav', 'nav', 'navbar-nav', 'pull-right');
        $mainMenu->setSizePolicy(\Func_Icons::ICON_LEFT_24);

        $page = $App->Ui()->Page();
        if (isset($itemId)) {
            $page->setId($itemId);
        }
        $page->addClass('depends-' . $this->getRecordClassName());

        $page->addToolbar($mainMenu);

        $page->addItem($fullFrame);

        $page->setReloadAction($this->proxy()->display($id, $view, $page->getId()));

        return $page;
    }


    /**
     * @param int   $id
     * @param int   $width
     * @param int   $height
     * @param string $itemId
     * @return \Widget_VBoxLayout
     */
    public function photo($id, $width = 400, $height = 200, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();

        $recordSet = $this->getRecordSet();
        $record = $recordSet->request($id);

        $Ui = $App->Ui();
        
        $photoItem = $Ui->ProjectPhoto($record, $width, $height, false, false);
        $photoItem->addClass('img-responsive');

        $box = $W->VBoxLayout($itemId);
        $box->addItem(
            $photoItem
        );

        if ($record->isUpdatable()) {
            $imagesFormItem = $W->ImagePicker();
            $imagesFormItem->oneFileMode();
            $imagesFormItem->hideFiles();

            $imagesFormItem->setAssociatedDropTarget($photoItem);

            $photoUploadPath = $record->getPhotoUploadPath();
            if (!is_dir($photoUploadPath->toString())) {
                $photoUploadPath->createDir();
            }
            $imagesFormItem->setFolder($photoUploadPath);
            $imagesFormItem->setAjaxAction($this->proxy()->cancel());

            $box->addItem(
                $imagesFormItem
            );
            $box->addClass('app-image-upload');
        }

        $box->setReloadAction($this->proxy()->photo($record->id, $width, $height, $box->getId()));

        return $box;
    }

    /**
     * @param \widget_TableModelView $tableView
     * @return \app_Toolbar
     */
    protected function toolbar(\widget_TableModelView $tableView)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $appC = $App->getComponentByName('Project');

        $toolbar = parent::toolbar($tableView);
        $toolbar->addItem(
            $W->Link(
                $appC->translate('Add project'),
                $this->proxy()->edit()
            )->setIcon(\Func_Icons::ACTIONS_LIST_ADD)->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );

        if($salesDocC = $App->getComponentByName('SALESDOCUMENT')){
            $toolbar->addItem(
                $W->Link(
                    $salesDocC->translate('Customer payment'),
                    $App->Controller()->Payment()->create()
                )->addClass('icon', \Func_Icons::ACTIONS_DOCUMENT_DOWNLOAD)
                ->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
            );
            $toolbar->addItem(
                $W->Link(
                    $salesDocC->translate('New sales order request'),
                    $App->Controller()->SalesOrder()->add()
                )->addClass('icon', \Func_Icons::ACTIONS_LIST_ADD)
                ->setOpenMode(\Widget_Link::OPEN_PAGE)
            );
        }
        
        $toolbar->addItem(
            $this->selectedItemsToolbar($tableView->getId())
        );

        return $toolbar;
    }
    
    /**
     *
     * @param string $modelViewId
     * @param string $itemId
     * @return \Widget_FlowLayout
     */
    public function selectedItemsToolbar($modelViewId, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        
        $modelView = $this->modelView(null, 'table');
        
        $box = $W->FlowLayout($itemId)->setHorizontalSpacing(1, 'ex');
        $box->addClass('depends-' . $modelViewId . '-selection');
        $box->setReloadAction($this->proxy()->selectedItemsToolbar($modelViewId, $box->getId()));
        $box->setIconFormat(16, 'left');
        
        $nbSelected = count($modelView->getSelectedRows());
        $links = array();
        
        $box->addItem(
            $W->Html(
                '<b>' . bab_toHtml($appC->translate('Selection:')) . '</b>'
            )
        );
        $box->addItem(
            $W->Link(
                $nbSelected,
                $this->proxy()->showSelectedRecords()
                )->setTitle($appC->translate('Show selected projects'))
            ->addClass('badge')
            ->setOpenMode(\Widget_Link::OPEN_DIALOG)
        );
        $box->addItem(
            $links[] = $W->Link(
                '',
                $this->proxy()->clearSelectedRecords()
            )->setIcon(\Func_Icons::ACTIONS_DIALOG_CANCEL)
            ->setTitle($appC->translate('Clear selection'))
            ->setAjaxAction()
        );
        $box->addItem(
            $W->Html(
                '<b>' . bab_toHtml($appC->translate('Actions:')) . bab_nbsp() . '</b>'
            )
        );
        
        $box->addItem(
            $links[] = $W->Link(
                $appC->translate('Delete'),
                $this->proxy()->deleteConfirmMultipleProject()
            )->setIcon(\Func_Icons::ACTIONS_EDIT_DELETE)
            ->setOpenMode(\Widget_Link::OPEN_DIALOG)
        );
        
        $box->addClass('widget-toolbar-multiselect', 'alert-info');
        
        if ($nbSelected == 0) {
            foreach($links as $link){
                $link->disable();
            }
        }
        return $box;
    }

    public function deleteConfirmMultipleProject()
    {
        $W = bab_Widgets();
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        $Ui = $App->Ui();
        
        $modelView = $this->modelView(null, 'table');
        $selectedRows = $modelView->getSelectedRows();
        
        $page = $Ui->Page();
        $page->setTitle(sprintf($appC->translate('Delete projects')));
        
        $layout = $W->VBoxItems()->setVerticalSpacing(2, 'em');
        $text = $W->Label($appC->translate(
                'Are you sure you want to remove this project ?',
                sprintf('Are you sure you want to remove those %d projects ?', count($selectedRows)),
                count($selectedRows)
            )
        );
        $layout->addItem($text);
        $page->addItem($layout);
        
        $editor = new \app_Editor($App, null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
        $editor->colon();
        $editor->setName('data');
        
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setSaveAction($this->proxy()->deleteMultiProject(), $appC->translate('Delete'));
        $editor->isAjax = $this->isAjaxRequest();
        
        $layout->addItem($editor);
        
        return $page;
    }

    public function deleteMultiProject($data = null)
    {
        $this->requireDeleteMethod();
        $App = $this->App();
        
        $modelView = $this->modelView(null, 'table');
        $selectedRows = $modelView->getSelectedRows();
        $projectSet = $App->ProjectSet();
        $projects = $projectSet->select(
            $projectSet->id->in($selectedRows)
        );
        foreach($projects as $p){
            $p->delete();
        }
        $this->clearSelectedRecords();
        return true;
    }

    public function clearSelectedRecords()
    {
        $modelView = $this->modelView(null, 'table');
        $modelView->clearSelectedRows();

        $this->addReloadSelector('.depends-' . $modelView->getId());

        return true;
    }
    
    public function previewHighlightedRecords($view = '', $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $modelView = $this->modelView();
        
        $highlightedRows = $modelView->getHighlightedRows();
        
        $recordSet = $this->getRecordSet();
        $records = $recordSet->select($recordSet->id->in($highlightedRows));
        
        $box = $W->VBoxLayout($itemId);
        $box->addClass('depends-' . $modelView->getId() . '-HighlightedRecords');
        $box->setReloadAction($this->proxy()->previewHighlightedRecords($view, $box->getId()));
        $box->setIconFormat(16, 'left');
        
        $Ui = $App->Ui();
        $recordClassname = $this->getRecordClassName();
        $viewClassname =  $recordClassname . 'FullFrame';
        
        if ($records->count() > 0) {
            $box->addItem(
                $W->Link(
                    '',
                    $this->proxy()->clearHighlightedRecords()
                )->setAjaxAction()
                ->setIcon(\Func_Icons::ACTIONS_ARROW_RIGHT_DOUBLE)
                ->addClass('app_closeHighlightButton')
                ->setSizePolicy('pull-right')
            );
            
            foreach ($records as $record) {
                $fullFrame = $Ui->$viewClassname($record);
                $fullFrame->setView("preview");
                $box->addItem($fullFrame);
            }
            $box->addClass('box', 'shadow');
        }
        
        return $box;
    }
    
    /**
     * Displays a form to edit the project status.
     * @param int   $project    The project id.
     * @return \app_Page
     */
    public function editStatus($project = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        
        $set = $this->getRecordSet();
        $set->customer();
        
        $project = $set->request($project);
        if (!$project->isUpdatable()) {
            throw new \app_AccessException($appC->translate('You are not allowed to access this page'));
        }
        
        $Ui = $App->Ui();
        
        $page = $Ui->Page();
        $page->setTitle($appC->translate('Edit project status'));
        
        $layout = $W->VBoxItems()->setVerticalSpacing(2, 'em');
        $page->addItem($layout);
        
        $layout->addItem(
            $W->Section(
                $appC->translate('Status history'),
                $this->statusHistory($project->id),
                5
            )->addClass('compact')
        );
        
        $editor = new \app_Editor($App, null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
        $editor->colon();
        
        $editor->setName('data');
        $editor->addItem($W->Hidden(null, 'id', $project->id));
        
        $projectStatusSet = $App->ProjectStatusSet();
        $projectStatuses = $projectStatusSet->select($projectStatusSet->parent->isNot(0));
        
        $statusOptions = array(0 => '');
        foreach ($projectStatuses as $projectStatus){
            $statusOptions[$projectStatus->id] = $projectStatus->name;
        }
        
        $editor->addItem(
            $W->Section(
                $appC->translate('New status'),
                $W->VBoxItems(
                    $W->FlowItems(
                        $editor->labelledField(
                            $appC->translate('Status'),
                            $W->Select()->setOptions($statusOptions),
                            'projectStatus'
                        )->setSizePolicy('widget-75pc'),
                        $editor->labelledField(
                            $appC->translate('Date'),
                            $W->DatePicker()
                            ->setMandatory(true, $appC->translate('You must specified the date'))
                            ->setValue((new \DateTime('now'))->format('Y-m-d')),
                            'date'
                        )->setSizePolicy('widget-25pc')
                    )->setHorizontalSpacing(2, 'em'),
                    $editor->labelledField(
                        $appC->translate('Comment'),
                        $W->TextEdit()
                        ->addClass('widget-100pc', 'widget-autoresize'),
                        'comment'
                    )
                )->setVerticalSpacing(1, 'em'),
                5
            )->addClass('compact')
        );
        
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setSaveAction($this->proxy()->saveStatus());
        
        $editor->isAjax = $this->isAjaxRequest();
        
        $layout->addItem($editor);
        
        return $page;
    }
    
    
    /**
     * Saves the project status.
     *
     * @param array $data ['id', 'status', 'comment', 'date']
     * @return bool
     */
    public function saveStatus($data = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        $set = $this->getRecordSet();
        $project = $set->request($data['id']);
        
        if (!$project->isUpdatable()) {
            throw new \app_AccessException($appC->translate('You are not allowed to access this page'));
        }
        
        $status = $data['projectStatus'];
        
        if (empty($status)) {
            return true;
        }
        
        $projectStatusHistorySet = $App->ProjectStatusHistorySet();
        
        $comment = $projectStatusHistorySet->comment->input($data['comment']);
        $date = $projectStatusHistorySet->date->input($data['date']);
        $project->updateStatus($status, $comment, $date);
        
        $this->addReloadSelector('.depends-project-status');
        
        return true;
    }
    
    /**
     * @param int $id Project id
     * @return \Widget_VBoxLayout
     */
    public function statusHistory($id, $itemId = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        
        $projectStatusHistorySet = $App->ProjectStatusHistorySet();
        $projectStatusHistorySet->projectStatus();
        
        $projectStatusHistories = $projectStatusHistorySet->select(
            $projectStatusHistorySet->project->is($id)
        )->orderDesc($projectStatusHistorySet->date)
        ->orderDesc($projectStatusHistorySet->id);
            
            
        $box = $W->VBoxLayout($itemId);
        $box->addClass(\Func_Icons::ICON_LEFT_16);
        $box->setVerticalSpacing(0, 'px');
        
        $table = $W->TableArrayView();
        $table->setPageLength(null);
        $table->setHeader(
            array(
                $appC->translate('Status'),
                $appC->translate('Date'),
                $appC->translate('Comment'),
                $appC->translate('By'),
                ''
            )    
        );
        $content = array();
        foreach ($projectStatusHistories as $projectStatusHistory) {
            $row = array();
            
            //Status
            $row[] = $W->FlowItems(
                $W->Frame()->setCanvasOptions(
                    \Widget_Item::Options()->backgroundColor('#' . $projectStatusHistory->projectStatus->color)
                )->addClass('app-color-preview'),
                $W->Label($projectStatusHistory->projectStatus->name)
            )->setHorizontalSpacing(1, 'em');
            
            //Date
            $row[] = $W->Label(
                bab_shortDate(bab_mktime($projectStatusHistory->date), false)
            );
            
            //Comment
            $row[] = $W->Label(
                bab_abbr($projectStatusHistory->comment, BAB_ABBR_FULL_WORDS, 100)
            )->addClass('widget-small');
            
            //By
            $userInitials = '';
            $userName = bab_getUserName($projectStatusHistory->modifiedBy, true);
            $userNameParts = preg_split("/[\s-]+/", $userName);
            foreach ($userNameParts as $userNamePart) {
                $userInitials .= substr($userNamePart, 0, 1);
            }
            $row[] = $W->Label(
                $userInitials
                )->setTitle(sprintf($appC->translate('Modified by %s on %s'), $userName, bab_shortDate(bab_mktime($projectStatusHistory->modifiedOn))));
            
            //Actions
            $row[] = $W->FlowItems(
                $W->Link(
                    '',
                    $this->proxy()->editStatusHistory($projectStatusHistory->id)
                )->setOpenMode(\Widget_Link::OPEN_DIALOG)
                ->setTitle($appC->translate('Edit'))
                ->addClass('icon', \Func_Icons::ACTIONS_DOCUMENT_EDIT),
                $W->Link(
                    '',
                    $this->proxy()->deleteStatusHistory($projectStatusHistory->id)
                )->addClass('icon', \Func_Icons::ACTIONS_EDIT_DELETE)
                ->setAjaxAction()
                ->setTitle($appC->translate('Delete'))
                ->setConfirmationMessage($appC->translate('Are you sure you want to delete this status?'))
            )->setSizePolicy('widget-align-right')
            ->addClass('widget-actions');
            
            $content[] = $row;
        }
        
        $table->setContent($content);
        $box->addItem($table);
        
        if(count($content) == 0){
            $box->addItem(
                $W->FlowItems($W->Label($appC->translate('No status history'))->setSizePolicy('alert alert-warning widget-100pc'))->addClass('widget-100pc')
            );
        }
                
        $box->setReloadAction($this->proxy()->statusHistory($id, $box->getId()));
        $box->addClass('depends-project-status');
        
        return $box;
    }
    
    public function editStatusHistory($projectStatusHistory = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        
        $projectStatusHistorySet = $App->ProjectStatusHistorySet();
        $projectStatusHistorySet->project();
        
        $projectStatusHistory = $projectStatusHistorySet->request($projectStatusHistory);
        if (!$projectStatusHistory->project->isUpdatable()) {
            throw new \app_AccessException($appC->translate('You are not allowed to access this page'));
        }
        
        $page = $App->Ui()->Page();
        
        $editor = new \app_Editor($App, null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));
        $editor->colon();
        
        $editor->setName('data');
        $editor->addItem($W->Hidden(null, 'id', $projectStatusHistory->id));
        
        $projectStatusSet = $App->ProjectStatusSet();
        $projectStatuses = $projectStatusSet->select($projectStatusSet->parent->isNot(0));
        
        $statusOptions = array(0 => '');
        foreach ($projectStatuses as $projectStatus){
            $statusOptions[$projectStatus->id] = $projectStatus->name;
        }
        
        $editor->addItem(
            $W->Section(
                $appC->translate('New status'),
                $W->VBoxItems(
                    $W->FlowItems(
                        $editor->labelledField(
                            $appC->translate('Status'),
                            $W->Select()->setOptions($statusOptions),
                            'projectStatus'
                        )->setSizePolicy('widget-75pc'),
                        $editor->labelledField(
                            $appC->translate('Date'),
                            $W->DatePicker()
                            ->setMandatory(true, $appC->translate('You must specified the date'))
                            ->setValue((new \DateTime('now'))->format('Y-m-d')),
                            'date'
                        )->setSizePolicy('widget-25pc')
                    )->setHorizontalSpacing(2, 'em'),
                    $editor->labelledField(
                        $appC->translate('Comment'),
                        $W->TextEdit()
                        ->addClass('widget-100pc', 'widget-autoresize'),
                        'comment'
                    )
                )->setVerticalSpacing(1, 'em'),
                5
            )->addClass('compact')
        );
        
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setValues($projectStatusHistory->getValues(), array('data'));
        $editor->setSaveAction($this->proxy()->saveStatusHistory());
        $editor->isAjax = $this->isAjaxRequest();
        
        $page->setTitle($appC->translate('Edit project status'));
        $page->addItem($editor);
        
        return $page;
    }
    
    public function saveStatusHistory($data = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        
        $projectStatusHistorySet = $App->ProjectStatusHistorySet();
        $projectStatusHistorySet->project();
        
        $projectStatusHistory = $projectStatusHistorySet->get($data['id']);
        if (!isset($projectStatusHistory)) {
            throw new \app_Exception($appC->translate('Trying to access a project status history with a wrong (unknown) id'));
        }
        
        if (!$projectStatusHistory->project->isUpdatable()) {
            throw new \app_AccessException($appC->translate('You are not allowed to access this page'));
        }
        
        $projectStatusHistory->setFormInputValues($data);
        $projectStatusHistory->save();
        
        $projectStatusHistory->project->updateLatestStatus();
        
        $this->addReloadSelector('.depends-project-status');
        
        return true;
    }
    
    /**
     * Deletes the specified project status history.
     * @param int $projectStatusHistory
     */
    public function deleteStatusHistory($projectStatusHistory)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        
        $projectStatusHistorySet = $App->ProjectStatusHistorySet();
        $projectStatusHistorySet->project();
        
        $projectStatusHistory = $projectStatusHistorySet->request($projectStatusHistory);
        if (!$projectStatusHistory->project->isUpdatable()) {
            throw new \app_AccessException($appC->translate('You are not allowed to access this page'));
        }
        
        $project = $projectStatusHistory->project;
        
        $projectStatusHistory->delete();
        
        $project->updateLatestStatus();
        
        $this->addReloadSelector('.depends-project-status');
        
        return true;
    }

	/**
	 * Returns a section with information and menus to edit/add
	 * quotations for a the specified project.
	 *
	 * @param Project|int $project The project.
	 *
	 * @return \Widget_Section
	 */
    public function quotationsTable($project = null, $itemId = null)
	{
		$W = bab_Widgets();
		$App = $this->App();
		$appC = $App->getComponentByName('Project');
		
		$salesDocC = $App->getComponentByName('QUOTATION');
		if(!isset($salesDocC)){
		    throw new \app_Exception(sprintf($App->translate('%s : the component %s must be installed to use this functionality'), __METHOD__, 'QUOTATION'));
		}

		if (!($project instanceof Project)) {
			$projectSet = $App->ProjectSet();
			/* @var $project Project */
			$project = $projectSet->get($project);
			if (!isset($project)) {
			    throw new \app_Exception($appC->translate('Trying to access a project with a wrong (unknown) id'));
			}
			if (!$project->isReadable()) {
			    throw new \app_AccessException($appC->translate('You are not allowed to access this page'));
			}
		}
		
		$box = $W->FlowLayout($itemId);
		$box->setIconFormat(16, 'left');
		$box->addClass('depends-quotations depends-Quotation');
		$box->setReloadAction($this->proxy()->quotationsTable($project->id, $box->getId()));
		
		/* @var $quotationSet QuotationSet */
		$quotationSet = $App->QuotationSet();
		$quotations = $quotationSet->select(
            $quotationSet->all(
                array(
                    $quotationSet->project->is($project->id)
                )    
            )
	    )->orderDesc($quotationSet->quotationDate);
	    
	    $quotationCtrl = $App->Controller()->Quotation();
		
	    $table = $W->TableArrayView();
	    $table->setSizePolicy('widget-100pc');
		$table->setHeader(
            array(
                $salesDocC->translate('Quotation date'),
                $salesDocC->translate('Number'),
                $salesDocC->translate('Customer reference'),
                $salesDocC->translate('Total VAT Excl.'),
                $salesDocC->translate('Total incl. tax'),
                $W->HBoxItems(
                    $W->Label($salesDocC->translate('Action')),
                    $W->Link(
                        '',
                        $quotationCtrl->edit(null, 'creation', array('project' => $project->id, 'billingOrganization' => $project->customer()->id, 'deliveryOrganization' => $project->customer()->id, 'payingOrganization' => $project->customer()->id))
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG)->addClass('widget-actionbutton')->setIcon(\Func_Icons::ACTIONS_LIST_ADD)
                )
            )    
	    );
		
		$content = array();
		
		$euroChar = $App->translate('_euro_');
		
		foreach($quotations as $quotation){
		    /* @var $quotation Quotation */
		    $row = array();
		    $row[] = $quotationSet->quotationDate->outputWidget($quotation->quotationDate);
		    $row[] = $W->Link($quotation->name, $quotationCtrl->display($quotation->id));
		    $row[] = $W->Link($quotation->customerReference, $quotationCtrl->display($quotation->id));
		    $row[] = $W->Label($App->numberFormat($quotation->totalDf).$euroChar)->setSizePolicy("widget-align-right");
		    $row[] = $W->Label($App->numberFormat($quotation->totalTi).$euroChar)->setSizePolicy("widget-align-right");
		    $row[] = $W->Link(
		        $appC->translate('Download'), 
		        $quotationCtrl->getPdf($quotation->id)
	        )->setOpenMode(\Widget_Link::OPEN_POPUP)->setIcon(\Func_Icons::MIMETYPES_APPLICATION_PDF);
		    $content[] = $row;
		}
		
		$table->setContent($content);
		
		$box->addItem($table);

		return $box;
	}


	private function newOrderLink(Project $deal, $label, $type)
	{
	    //@TODO : following commented lines must be rework once the component SALESDOCUMENT is done
// 	    $W = bab_Widgets();
// 	    $Crm = $this->Crm();

// 	    return
// 	        $W->Link(
// 	            $label,
// 	            $Crm->Controller()->Order()->confirmAdd($deal->id, $type)
//             )
// 	        ->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
// 	        ->addClass('icon')
// 	        ->addClass(Func_Icons::ACTIONS_LIST_ADD);
	}
	
    protected function menu(\Widget_Page $page, Project $project, $current = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        $attachmentC = $App->getComponentByName('ATTACHMENT');
        $page->addItemMenu('project', $appC->translate('Project'), $this->proxy()->display($project->id)->url());
        $page->addItemMenu('attachments', $attachmentC->translate('Attached files'), $App->Controller()->Attachment()->attachments($project->getRef())->url());
        if (isset($current)) {
            $page->setCurrentItemMenu($current);
        }
    }


	/**
	 * Saves/updates a project
	 *
	 * @param array		$data		An indexed array containing the data to store in the project
	 *
	 * @return \Widget_Action
	 */
    public function save($data = null)
	{
        $this->requireSaveMethod();
        
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
		$set = $App->ProjectSet();
		$set->address();

		if (!empty($data['id'])) {
			$record = $set->get($data['id']);
			if (!isset($record)) {
			    throw new \app_AccessException($appC->translate('Trying to access a project with a wrong (unknown) id'));
			}
		} else {
			$record = $set->newRecord();
		}
		
		/* @var $record Project */
		$record->setFormInputValues($data);
		$newDeal = !isset($record->id);

		if (isset($data['referralContact_name'])) {
		    if (trim($data['referralContact_name']) === '') {
				$record->referralContact = 0;
			} else {
				// A referral contact name has been specified.
				// We try to find him/her in the contacts...
				$contactSet = $App->ContactSet();
				list($namestart) = explode(' ', $data['referralContact_name']);
				$referralContacts = $contactSet->select($contactSet->lastname->startsWith($namestart)->_OR_($contactSet->firstname->startsWith($namestart)));
				foreach ($referralContacts as $referralContact) {
					if ($referralContact->firstname . ' ' . $referralContact->lastname == $data['referralContact_name']) {
						$record->referralContact = $referralContact->id;
						break;
					}
				}
			}
		}

		if (isset($data['responsible_name'])) {
		    if (trim($data['responsible_name']) === '') {
				$record->responsible = 0;
		    } else {
		        $contactSet = $App->ContactSet();
				list($namestart) = explode(' ', $data['responsible_name']);
				$responsibleContacts = $contactSet->select($contactSet->lastname->startsWith($namestart)->_OR_($contactSet->firstname->startsWith($namestart)));
				foreach ($responsibleContacts as $responsibleContact) {
					$responsibleName = $responsibleContact->getFullName();
					if ($responsibleName == $data['responsible_name']) {
						$record->responsible = $responsibleContact->id;
						break;
					}
				}
			}
		}
		
		$record->save();
		
		if($newDeal){
		    $customer = $record->customer();
		    if($customer){
		        $customerAddress = $customer->address();
		        if($customerAddress){
		            $values = $customerAddress->getFormOutputValues();
		            unset($values['id']);
		            unset($values['uuid']);
		            unset($values['createdOn']);
		            unset($values['createdBy']);
		            unset($values['modifiedOn']);
		            unset($values['modifiedBy']);
		            unset($values['deletedOn']);
		            unset($values['deletedBy']);
		            $record->address->setFormInputValues($values);
		            $record->address->save();
		        }
		    }
		}
		
		$record->getPhotoUploadPath()->createDir();

		$nextMessage = $appC->translate('The project has been saved');
		$redirectAction = $this->proxy()->display($record->id);
		if($newDeal){
		    $nextMessage = $appC->translate('The project has been created');
		    $redirectAction = $this->proxy()->display($record->id, true);
		}
		
		if(bab_isAjaxRequest()){
		    $this->addMessage($nextMessage);
		    $this->addReloadSelector('.depends-' . $this->getRecordClassName());
		    return true;
		}
		
		app_redirect($redirectAction, $nextMessage);
	}

	
	/**
	 * Search in all projects based on the name, number and fullNumber
	 *
	 * @param string $q
	 * @return string (json)
	 */
	public function search($q = null)
	{
	    $App = $this->App();
	    $appC = $App->getComponentByName('Project');
	    $set = $this->getRecordSet();
	    $set->addFullNumberField();
	    $set->customer();
	    $W = bab_Widgets();
	    $W->includePhpClass('Widget_Select2');
	    $collection = new \Widget_Select2OptionsCollection();
	    
	    if(strlen($q)<3){
	        $collection->addOption(new \Widget_Select2Option(0, $appC->translate("The research is too short")));
	        header("Content-type: application/json; charset=utf-8");
	        echo $collection->output();
	        die();
	    }
	    $projects = $set->select(
	        $set->any(
	            array(
	                $set->name->contains($q),
	                $set->number->contains($q),
	                $set->fullNumber->contains($q)
	            )
            )
        )->orderAsc($set->name);
	        
        $counter = 0;
        foreach($projects as $project){
            $counter++;
            $collection->addOption(
                new \Widget_Select2Option($project->id, $project->name.'('.$project->fullNumber.')', $project->customer()->name)
            );
        }
        
        header("Content-type: application/json; charset=utf-8");
        echo $collection->output();
        die();
	}
	




    /**
     * Display expenses and incomes
     * @param int $deal
     */
    public function expensesIncomes($deal = null)
    {
        //@TODO : following commented lines must be rework once the component SALESDOCUMENT is done
//         $W = crm_Widgets();
//         $Crm = $this->Crm();
//         $Ui = $Crm->Ui();
//         $Access = $Crm->Access();
//         /*@var $Ui lcrm_Ui */


//         $dealSet = $Crm->DealSet();
//         /* @var $deal crm_Deal */
//         $deal = $dealSet->get($deal);
//         if (!isset($deal)) {
//             throw new crm_AccessException(lcrm_translate('Trying to access a deal with a wrong (unknown) id.'));
//         }
//         if (!$Access->readDeal($deal)) {
//             throw new crm_AccessException($Crm->translate('You are not allowed to access this page.'));
//         }

//         $page = $Ui->Page();
//         $title = sprintf(lcrm_translate('Budget for "%s"'), $deal->name);

//         crm_BreadCrumbs::setCurrentPosition($this->proxy()->expensesIncomes($deal->id), $title);
//         $page->setTitle($title);


//         $mainFrame = $W->Frame()->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));
//         $mainFrame->addClass('crm-detailed-info-frame');
//         $page->addItem($mainFrame);

//         $mainFrame->addItem($W->Title($title, 1));
//         $mainFrame->addItem($Ui->DealExpensesIncomesTableView($deal));

//         $page->addContextItem($Ui->DealCardFrame($deal));

//         return $page;
    }




    /**
     *
     * @param array $monthlyValues
     * @return \Widget_TableView
     */
    protected function tv($periodValues, $periodDeals)
    {
        //@TODO : following commented lines must be rework once the component SALESDOCUMENT is done
//         $Crm = $this->Crm();
//         $W = bab_Widgets();
//         $tableView = $W->TableView();

//         $periodLabels = array(
//             0 => $Crm->translate('Sales amount'),
//             1 => $Crm->translate('Costs'),
//             2 => $Crm->translate('Margin')
//         );

//         $tableView->addSection('header', null, 'widget-table-header');
//         $tableView->setCurrentSection('header');

//         $row = 0;
//         $col = 0;
//         $tableView->addItem($W->Label(''), $row, $col);
//         $col++;
//         $previousYear = null;
//         foreach ($periodValues as $monthlyValues) {
//             foreach ($monthlyValues as $yearMonth => $value) {
//                 list($year) = explode('-', $yearMonth);
//                 $time = bab_mktime($yearMonth . '-01');
//                 if ($previousYear !== $year) {
//                     $monthLabel = date('M Y', $time);
//                     $monthLabel = bab_formatDate('%m' . "\n". '%y', $time);
//                     $previousYear = $year;
//                 } else {
//                     $monthLabel = date('M', $time);
//                     $monthLabel = bab_formatDate('%m', $time);
//                 }

//                 $tableView->addItem($W->Label($monthLabel), $row, $col++);
//             }
//             break;
//         }

//         $tableView->addSection('body', null, 'widget-table-body');
//         $tableView->setCurrentSection('body');

//         $row++;

//         foreach ($periodValues as $periodId => $monthlyValues) {

//             $monthlyDeals = $periodDeals[$periodId];

//             $col = 0;
//             $tableView->addItem($W->Label($periodLabels[$periodId]), $row, $col);
//             $col++;

//             foreach ($monthlyValues as $yearMonth => $value) {

//                 if (count($monthlyDeals[$yearMonth]) > 0) {
//                     $item = $W->Link(
//                         '' . $value,
//                         $Crm->Controller()->Deal()->displayList(
//                             array(
//                                 'filter' => array(
//                                     'fullNumber' => implode(',', $monthlyDeals[$yearMonth])
//                                 )
//                             )
//                         )
//                     )->setTitle(count($monthlyDeals[$yearMonth]));
//                 } else {
//                     $item = $W->Label('' . $value);
//                 }

//                 $tableView->addItem($item, $row, $col);
//                 $col++;
//             }
//             $row++;
//         }

//         return $tableView;
    }



    public function salesMarginChart($nbPeriods = 12, $view = null, $itemId = null)
    {
        //@TODO : following commented lines must be rework once the component SALESDOCUMENT is done
//         $Crm = $this->Crm();

// //        $nbYears = 1;

//         if (!isset($view)) {
//             $view = Widget_TableView::VIEW_CHART_STACKEDBAR;
//         }

//         $Crm->includeDealSet();

//         $periodValues = array();
//         $periodDeals = array();

// //        for ($year = $nbYears - 1; $year >= 0; $year--) {

//             $monthlyAmounts = array();
//             $monthlyMargins = array();
//             $monthlyCosts = array();
//             $monthlyDeals = array();

//             for ($period = $nbPeriods - 1; $period >= 0; $period--) {

//                 $endDate = BAB_DateTime::fromIsoDateTime(date('Y-m-') . '01 00:00:00');
//                 //$endDate->add(-$year, BAB_DATETIME_YEAR);
//                 $endDate->add(-$period, BAB_DATETIME_MONTH);
//                 $startDate = $endDate->cloneDate();
//                 $startDate->add(-1, BAB_DATETIME_MONTH);


//                 $start = $startDate->getIsoDate();
//                 $end = $endDate->getIsoDate();

//                 $dateYear = sprintf('%4d-%02d', $startDate->getYear(), $startDate->getMonth());

//                 if (!isset($monthlyAmounts[$dateYear])) {
//                     $monthlyAmounts[$dateYear] = 0;
//                 }
//                 if (!isset($monthlyCosts[$dateYear])) {
//                     $monthlyCosts[$dateYear] = 0;
//                 }
//                 if (!isset($monthlyMargins[$dateYear])) {
//                     $monthlyMargins[$dateYear] = 0;
//                 }
//                 if (!isset($monthlyDeals[$dateYear])) {
//                     $monthlyDeals[$dateYear] = array();
//                 }

//                 $dealStatusHistorySet = $Crm->DealStatusHistorySet();
//                 $dealStatusHistorySet->status();

//                 $dealStatusHistoryWon = $dealStatusHistorySet->all(
//                     $dealStatusHistorySet->date->greaterThanOrEqual($start),
//                     $dealStatusHistorySet->date->lessThan($end),
//                     $dealStatusHistorySet->status->reference->is(lcrm_Deal::STATUS_WON)
//                 );

//                 $orderSet = $Crm->OrderSet();
//                 $orderSet->deal();
//                 $orderSet->deal->addFullNumberField();

//                 $orders = $orderSet->select(
//                     $orderSet->all(
//                         $orderSet->type->is(lcrm_Order::ORDER),
//                         $orderSet->deal->id->in($dealStatusHistoryWon, 'deal')
//                     )
//                 );

//                 foreach ($orders as $order) {
//                     $monthlyAmounts[$dateYear] += $order->total_df;
//                     $monthlyCosts[$dateYear] += $order->getProvisionalCost();
//                     $monthlyMargins[$dateYear] += $order->total_df - $order->getProvisionalCost();
//                     $monthlyDeals[$dateYear][] = $order->deal->fullNumber;
//                 }
//             }

//             if ($view == Widget_TableView::VIEW_DETAILS) {
//                 $periodValues[0] = $monthlyAmounts;
//             }
//             $periodValues[1] = $monthlyCosts;
//             $periodValues[2] = $monthlyMargins;
//             if ($view == Widget_TableView::VIEW_DETAILS) {
//                 $periodDeals[0] = $monthlyDeals;
//             }
//             $periodDeals[1] = $monthlyDeals;
//             $periodDeals[2] = $monthlyDeals;
// //        }

//         $tableview = $this->tv($periodValues, $periodDeals);

//         $tableview->setView($view);
//         //         $tableView->setView(Widget_TableView::VIEW_CHART_LINE);
//         //         $tableView->setView(Widget_TableView::VIEW_CHART_BAR);

//         return $tableview;
    }



    public function confirmRenew($deal = null)
    {
        //@TODO : following commented lines must be rework once the component SALESDOCUMENT is done
//         $W = bab_Widgets();

//         $Crm = $this->Crm();
//         $Ui = $Crm->Ui();
//         $set = $Crm->DealSet();
//         $dealRecord = $set->request($deal);
//         $date = $dealRecord->getNextOrderDate();

//         if (!isset($date)) {
//             throw new crm_AccessException('Next order date is required');
//         }

//         $page = $Ui->Page();

//         $page->addClass('crm-page-editor');
//         $page->setTitle($Crm->translate('Renew order'));

//         $form = new crm_Editor($Crm);
//         $form->addItem($W->Title($Crm->translate('Confirm deal and order creation?'), 6));
//         $form->addItem($W->Label(sprintf($Crm->translate('Date: %s'), $date->format('d/m/Y') )));

//         $form->setHiddenValue('tg', bab_rp('tg'));
//          $form->setHiddenValue('deal', $deal);

//         $form->addButton(
//             $W->SubmitButton()
//             ->setAction($this->proxy()->renew())
//             ->setLabel($Crm->translate('Renew'))
//             );
//         $form->addButton($W->SubmitButton()->setLabel($Crm->translate('Cancel'))->addClass('widget-close-dialog'));
//         $page->addItem($form);

//         return $page;
    }


    public function renew($deal = null)
    {
        //@TODO : following commented lines must be rework once the component SALESDOCUMENT is done
//         $Crm = $this->Crm();
//         $set = $Crm->DealSet();
//         $dealRecord = $set->request($deal);

//         if ($dealRecord->isRenewed()) {
//             throw new crm_SaveException(lcrm_translate('The order is allready renewed'));
//         }

//         $dealRecord->renewOrder();

//         $babBody = bab_getBody();
//         $babBody->addNextPageMessage(lcrm_translate('A deal with the associated order has been created'));

//         return true;
    }
    
    public function displayStatus($projectId, $editable = true, $itemId = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        $W = bab_Widgets();
        
        $statusBox = $W->VBoxLayout($itemId)->setIconFormat(16, 'left');
        $statusBox->setReloadAction($this->proxy()->displayStatus($projectId, $editable, $statusBox->getid()));
        $statusBox->addClass('depends-project-status');
        $statusBox->setIconFormat();
        
        $projectSet = $App->ProjectSet();
        $project = $projectSet->get($projectSet->id->is($projectId));
        
        if(!$project){
            return $statusBox;
        }
        
        $statusBox->addItem(
            $statusItem = $W->HBoxItems()
            ->setHorizontalSpacing(0.5, 'em')
            ->setVerticalAlign('middle')
        );
        
        $status = $project->status();
        
        $projectStatusHistorySet = $App->ProjectStatusHistorySet();
        $projectPreviousStatuses = $projectStatusHistorySet->select($projectStatusHistorySet->project->is($project->id));
        $projectPreviousStatuses->orderDesc($projectStatusHistorySet->date);
        
        $statusDate = '';
        foreach ($projectPreviousStatuses as $projectPreviousStatus) {
            $statusDate = $projectPreviousStatus->date;
            break;
        }
        
        if($status){
            $statusItem->addItems(
                $W->Frame()->setCanvasOptions(
                    \Widget_Item::Options()->backgroundColor('#' . $status->color)
                )->addClass('app-color-preview'),
                $W->Label($status->name)
            );
            if(!empty($statusDate) && $statusDate != '0000-00-00'){
                $statusItem->addItem(
                    $W->Label('(' . bab_shortDate(bab_mktime($statusDate), false) . ')')
                );
            }
        }
        else{
            $statusItem->addItem(
                $W->Label($appC->translate('No status'))
            );
        }
        if($editable){
            $statusItem->addItem(
                $W->Link(
                    '',
                    $this->proxy()->editStatus($project->id)
                )->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT)
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
                ->addClass('widget-actionbutton section-button')
            );
        }
        
        return $statusBox;
    }
}
