<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Project\Ctrl;

use Capwelton\App\Project\Set\ProjectClassificationSet;

$App = app_App();
$App->includeRecordController();
$App->setCurrentComponentByName('Project');

/**
 * This controller manages actions that can be performed on project classificationes.
 *
 * @method \Func_App    App()
 */
class ProjectClassificationController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('ProjectClassification'));
    }
    
    /**
     * @return \app_Page
     */
    public function displayList()
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $page = $App->Ui()->Page();
        $page->addClass('app-page-editor');
        
        $page->addItem($W->Title($App->translate('Project classifications'), 1));
        
        $Ui = $App->Ui();
        
        $page->addItem($Ui->ProjectClassificationList());
        $page->setReloadAction($this->proxy()->displayList());
        $page->addClass('depends-projectClassificationList');
        
        return $page;
    }
    
    /**
     * @return \app_Page
     */
    public function add($parentClassification)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('ProjectClassification');
        $W = bab_Widgets();
        
        $classificationSet = $App->ProjectClassificationSet();
        
        $page = $App->Ui()->Page();
        $page->addClass('app-page-editor');
        
        $page->addItem($W->Title($appC->translate('Add classification'), 1));
        
        $Ui = $App->Ui();
        
        $classificationEditor = $Ui->ProjectClassificationEditor();
        $classificationEditor->setName('classification');
        
        $parentClassification = $classificationSet->get($parentClassification);
        if (isset($parentClassification->id)) {
            $classificationEditor->addItem(
                $W->Hidden(null, 'parent', $parentClassification->id)
            );
        }
        
        $classificationEditor->setSaveAction($this->proxy()->save());
        $classificationEditor->setCancelAction($this->proxy()->cancel());
        
        $page->addItem($classificationEditor);
        
        return $page;
    }
    
    /**
     * @return \app_Page
     */
    public function edit($classification)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('ProjectClassification');
        $W = bab_Widgets();
        
        $classificationSet = $App->ProjectClassificationSet();
        $classification = $classificationSet->get($classification);
        
        $page = $App->Ui()->Page();
        $page->addClass('app-page-editor');
        
        $page->addItem($W->Title($appC->translate('Edit classification'), 1));
        
        $Ui = $App->Ui();
        
        $classificationEditor = $Ui->ProjectClassificationEditor();
        $classificationEditor->setName('classification');
        if (isset($classification)) {
            $classificationEditor->addItem(
                $W->Hidden(null, 'id', $classification->id)    
            );
            $classificationEditor->setRecord($classification);
        }
        
        $classificationEditor->setSaveAction($this->proxy()->save());
        $classificationEditor->setCancelAction($this->proxy()->cancel());
        
        $page->addItem($classificationEditor);
        
        return $page;
    }
    
    /**
     * @param array $teamType
     * @return boolean
     */
    public function save($classification = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('ProjectClassification');
        
        $data = $classification;
        $projectClassificationSet = $App->ProjectClassificationSet();
        
        if (isset($data['id'])) {
            $projectClassification = $projectClassificationSet->request($data['id']);
            $projectClassification->setFormInputValues($data);
            $projectClassification->save();
        } else {
            $projectClassification = $projectClassificationSet->newRecord();
            $projectClassification->setFormInputValues($data);
            $parent = $projectClassificationSet->get($data['parent']);
            $parent->appendChild($projectClassification);
        }
        
        $this->addMessage($appC->translate('The project classification has been saved'));        
        app_redirect($this->proxy()->displayList());
        
        return true;
    }
    
    /**
     * Displays a page asking to confirm the deletion of a record.
     *
     * @param int $id
     */
    public function confirmDelete($id)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $appC = $App->getComponentByName('ProjectClassification');
        $Ui = $App->Ui();
        
        $recordSet = $this->getRecordSet();
        $record = $recordSet->get($id);
        
        $page = $Ui->Page();
        
        $page->addClass('app-page-editor');
        $page->setTitle($appC->translate('Deletion'));
        
        if (!isset($record)) {
            $page->addItem(
                $W->Label($appC->translate('This element does not exists'))
            );
            $page->addClass('alert', 'alert-warning');
            return $page;
        }
        
        $form = new \app_Editor($App);
        
        $form->addItem($W->Hidden()->setName('id'));
        
        $recordTitle = $record->getRecordTitle();
        
        $subTitle = $appC->translate($recordSet->getDescription()) . ' "' . $recordTitle . '"';
        $form->addItem($W->Title($subTitle, 5));
        
        $form->addItem($W->Title($appC->translate('Confirm delete?'), 6));
        
        $form->addItem($W->Html(bab_toHtml($appC->translate('It will not be possible to undo this deletion'))));
        
        $confirmedAction = $this->proxy()->delete($id);
        $parameters = $confirmedAction->getParameters();
        foreach ($parameters as $key => $value) {
            $form->setHiddenValue($key, $value);
        }
        $form->addButton(
            $W->SubmitButton()
            ->setAction($confirmedAction)
            ->setLabel($appC->translate('Delete'))
        );
        $page->addItem($form);
        
        return $page;
    }
    
    public function delete($classification)
    {
        $this->requireDeleteMethod();
        $App = $this->App();
        $appC = $App->getComponentByName('ProjectClassification');
        
        $set = $App->ProjectClassificationSet();        
        $classification = $set->request($set->id->is($classification));
        $set->delete($set->id->is($classification->id));
        
        $this->addMessage($appC->translate('The project classification has been deleted')); 
        app_redirect($this->proxy()->displayList());
        
        return true;
    }
    
    /**
     * Does nothing and returns to the previous page.
     * @return bool
     */
    public function cancel()
    {
        return true;
    }
}