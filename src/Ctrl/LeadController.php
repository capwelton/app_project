<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Project\Ctrl;

use Capwelton\App\Project\Set\ProjectClassificationSet;
use Capwelton\App\Project\Set\Lead;

$App = app_App();
$App->includeRecordController();
$App->setCurrentComponentByName('Project');

/**
 * This controller manages actions that can be performed on leads.
 *
 * @method \Func_App    App()
 */
class LeadController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('Lead'));
    }
    
    /**
     * Does nothing and returns to the previous page.
     * @return bool
     */
    public function cancel()
    {
        return true;
    }
    
    protected function toolbar(\widget_TableModelView $tableView)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $appC = $App->getComponentByName('Lead');
        
        $toolbar = parent::toolbar($tableView);
        $toolbar->addItem(
            $W->Link(
                $appC->translate('Add lead'),
                $this->proxy()->edit()
            )->setIcon(\Func_Icons::ACTIONS_LIST_ADD)->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );
        return $toolbar;
    }
    
    public function save($data = null)
    {
        $this->requireSaveMethod();
        
        $App = $this->App();
        $set = $App->LeadSet();
        
        if(isset($data['id']) && !empty($data['id'])){
            $record = $set->request($set->id->is($data['id']));
        }
        else{
            $record = $set->newRecord();
        }
        
        if(isset($data['source']) && $data['source'] == 'new' && isset($_SESSION['LeadNewSource']) && !empty($_SESSION['LeadNewSource'])){
            $sourceSet = $App->LeadSourceSet();
            $source = $sourceSet->newRecord();
            $source->name = $_SESSION['LeadNewSource'];
            $source->save();
            $data['source'] = $source->id;
        }
        
        $record->setFormInputValues($data);
        $record->save();
        
        
        $this->addReloadSelector('.depends-' . $this->getRecordClassName());
        
        return true;
    }
    
    /**
     * @param Lead $record
     * @return array
     */
    protected function getActions(Lead $record)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Lead');
        
        $ctrl = $this->proxy();
        $actions = array();
        $moreActions = array();
        $documentsActions = array();
        
        
        if ($record->isUpdatable()) {
            $action = $ctrl->edit($record->id);
            $action->setTitle($appC->translate('Edit'));
            $action->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT);
            $actions['edit'] = $action;
        }
        
        if ($record->isReadable()) {
            $action = $App->Controller()->Attachment()->attachments($record->getRef());
            $action->setTitle($appC->translate('Attached files'));
            $action->setIcon(\Func_Icons::APPS_FILE_MANAGER);
            $documentsActions['attachedFiles'] = $action;
        }
        
        
        if ($record->isDeletable()) {
            $action = $ctrl->confirmDelete($record->id);
            $action->setTitle($appC->translate('Delete'));
            $action->setIcon(\Func_Icons::ACTIONS_EDIT_DELETE);
            $moreActions['delete'] = $action;
        }
        
        if (bab_isUserAdministrator()) {
            $customContainerCtrl = $App->Controller()->CustomContainer();
            $views = $record->getViews();
            $className = $record->getClassName();
            
            foreach ($views as $view) {
                $action = $customContainerCtrl->editContainers($className, $view);
                $action->setTitle(sprintf($App->translate('Edit view layout %s'), $view));
                $action->setIcon(\Func_Icons::ACTIONS_VIEW_PIM_JOURNAL);
                $moreActions['editSections_' . $view] = $action;
            }
            
            $action = $customContainerCtrl->importContainersConfirm($className, $view);
            $action->setTitle($App->translate('Import view layout'));
            $action->setIcon(\Func_Icons::ACTIONS_VIEW_PIM_JOURNAL);
            $moreActions['importSectionsConfirm'] = $action;
        }
        
        $actions['documents'] = array(
            'icon' => \Func_Icons::APPS_FILE_MANAGER,
            'title' => 'Documents',
            'items' => array(
                $documentsActions
            )
        );
        $actions['more'] = array(
            'icon' => 'actions-context-menu'/*Func_Icons::ACTIONS_CONTEXT_MENU*/,
            'title' => 'More',
            'items' => array(
                $moreActions
            )
        );
        return $actions;
    }
}