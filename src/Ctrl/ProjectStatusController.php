<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Project\Ctrl;

use Capwelton\App\Project\Set\ProjectStatusSet;

$App = app_App();
$App->includeRecordController();
$App->setCurrentComponentByName('Project');

/**
 * This controller manages actions that can be performed on project statuses.
 *
 * @method \Func_App    App()
 */
class ProjectStatusController extends \app_ComponentCtrlRecord
{
    /**
     * @isComponentController
     */
    public function __construct(\Func_App $app)
    {
        parent::__construct($app, $app->getComponentByName('ProjectStatus'));
    }
    /**
     * @return \app_Page
     */
    public function displayList()
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        $W = bab_Widgets();
        
        $page = $App->Ui()->Page();
        $page->addClass('app-page-editor');
        
        $page->addItem($W->Title($appC->translate('Project statuses'), 1));
        $Ui = $App->Ui();
        
        $page->addItem($Ui->ProjectStatusList());
        $page->setReloadAction($this->proxy()->displayList());
        $page->addClass('depends-projectStatusList');
        
        return $page;
    }
    
    /**
     * @return \app_Page
     */
    public function add($parentStatus)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        $W = bab_Widgets();
        
        $statusSet = $App->ProjectStatusSet();
        
        $page = $App->Ui()->Page();
        $page->addClass('app-page-editor');
        
        $page->addItem($W->Title($appC->translate('Add status'), 1));
        $Ui = $App->Ui();
        
        $statusEditor = $Ui->ProjectStatusEditor();
        $statusEditor->setName('status');
        
        $parentStatus = $statusSet->get($parentStatus);
        if (isset($parentStatus->id)) {
            $statusEditor->addItem(
                $W->Hidden(null, 'parent', $parentStatus->id)
            );
        }
        
        $statusEditor->setSaveAction($this->proxy()->save());
        $statusEditor->setCancelAction($this->proxy()->cancel());
        
        $page->addItem($statusEditor);
        
        return $page;
    }
    
    /**
     * @return \app_Page
     */
    public function edit($status)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        $W = bab_Widgets();
        
        $statusSet = $App->ProjectStatusSet();
        $status = $statusSet->get($status);
        
        $page = $App->Ui()->Page();
        $page->addClass('app-page-editor');
        
        $page->addItem($W->Title($appC->translate('Edit status'), 1));
        $Ui = $App->Ui();
        
        $statusEditor = $Ui->ProjectStatusEditor();
        $statusEditor->setName('status');
        if (isset($status)) {
            $statusEditor->addItem(
                $W->Hidden(null, 'id', $status->id)    
            );
            $statusEditor->setRecord($status);
        }
        
        $statusEditor->setSaveAction($this->proxy()->save());
        $statusEditor->setCancelAction($this->proxy()->cancel());
        
        $page->addItem($statusEditor);
        
        return $page;
    }
    
    /**
     * @param array $teamType
     * @return boolean
     */
    public function save($status = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        
        $data = $status;
        $projectStatusSet = $App->ProjectStatusSet();
        
        if (isset($data['id'])) {
            $projectStatus = $projectStatusSet->request($data['id']);
            $projectStatus->setFormInputValues($data);
            $projectStatus->save();
        } else {
            $projectStatus = $projectStatusSet->newRecord();
            $projectStatus->setFormInputValues($data);
            $parent = $projectStatusSet->get($data['parent']);
            $parent->appendChild($projectStatus);
        }
        
        $this->addMessage($appC->translate('The project status has been saved'));        
        app_redirect($this->proxy()->displayList());
        
        return true;
    }
    
    /**
     * Displays a page asking to confirm the deletion of a record.
     *
     * @param int $id
     */
    public function confirmDelete($id)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        $Ui = $App->Ui();
        
        $recordSet = $this->getRecordSet();
        $record = $recordSet->get($id);
        
        $page = $Ui->Page();
        
        $page->addClass('app-page-editor');
        $page->setTitle($appC->translate('Deletion'));
        
        if (!isset($record)) {
            $page->addItem(
                $W->Label($appC->translate('This element does not exists'))
            );
            $page->addClass('alert', 'alert-warning');
            return $page;
        }
        
        $form = new \app_Editor($App);
        
        $form->addItem($W->Hidden()->setName('id'));
        
        $recordTitle = $record->getRecordTitle();
        
        $subTitle = $appC->translate($recordSet->getDescription()) . ' "' . $recordTitle . '"';
        $form->addItem($W->Title($subTitle, 5));
        
        $form->addItem($W->Title($appC->translate('Confirm delete?'), 6));
        
        $form->addItem($W->Html(bab_toHtml($appC->translate('It will not be possible to undo this deletion'))));
        
        $confirmedAction = $this->proxy()->delete($id);
        $parameters = $confirmedAction->getParameters();
        foreach ($parameters as $key => $value) {
            $form->setHiddenValue($key, $value);
        }
        $form->addButton(
            $W->SubmitButton()
            ->setAction($confirmedAction)
            ->setLabel($appC->translate('Delete'))
        );
        $page->addItem($form);
        
        return $page;
    }
    
    public function delete($status)
    {
        $this->requireDeleteMethod();
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        
        $set = $App->ProjectStatusSet();        
        $status = $set->request($set->id->is($status));
        $set->delete($set->id->is($status->id));
        
        $this->addMessage($appC->translate('The project status has been deleted')); 
        app_redirect($this->proxy()->displayList());
        
        return true;
    }

    // Allow list reorder by switching this item's place with the one ahead
    public function moveUp($status)
    {
        $App = $this->App();
        $statusSet = $App->ProjectStatusSet();
        $status = $statusSet->get($status);
        if ($status->lf > 1) {
            $currentLf = $status->lf;
            while ($currentLf !== 1) {
                $currentLf = $currentLf - 1;
                $previous = $statusSet->get($statusSet->lf->is($currentLf));
                if (isset($previous)) {
                    $lf = $previous->lf;
                    $previous->lf = $status->lf;
                    $previous->lr = $status->lf + 1;
                    $previous->save();
                    $status->lf = $lf;
                    $status->lr = $lf +1;
                    $status->save();
                    break;
                }
            }
        }
        $this->addReloadSelector('.depends-projectStatusList');
        return true;
    }
    
    /**
     * Does nothing and returns to the previous page.
     * @return bool
     */
    public function cancel()
    {
        return true;
    }
}
