<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Project\Ctrl;

use Capwelton\App\Project\Set\LeadSourceSet;

$App = app_App();
$App->includeRecordController();
$App->setCurrentComponentByName('Project');


/**
 * This controller manages actions that can be performed on lead sources.
 *
 * @method \Func_App App()
 */
class LeadSourceController extends \app_ComponentCtrlRecord
{
    protected function toolbar(\widget_TableModelView $tableView)
    {
        $W = bab_Widgets();
        $App = $this->App();
        $appC = $App->getComponentByName('Lead');
        
        $toolbar = parent::toolbar($tableView);
        $toolbar->addItem(
            $W->Link(
                $appC->translate('Add lead source'),
                $this->proxy()->edit()
            )->setIcon(\Func_Icons::ACTIONS_LIST_ADD)->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );
        return $toolbar;
    }
    
    public function search($q = null){
        $W = bab_Widgets();
        $App = $this->App();
        $appC = $App->getComponentByName('Lead');
        $set = $App->LeadSourceSet();
        
        $W->includePhpClass('Widget_Select2');
        $collection = new \Widget_Select2OptionsCollection();
        $counter = 0;
        $criteria = array();
        $criteria[] = $set->any(
            $set->name->contains($q)
        );
        $sources = $set->select(
            $set->all($criteria)
        );
        
        foreach($sources as $source){
            $counter++;
            if($counter >10){
                break;
            }
            $collection->addOption(
                new \Widget_Select2Option($source->id, $source->name)
            );
        }
        
        if(!empty($q)){
            $collection->addOption(
                new \Widget_Select2Option('new', $q, $appC->translate('New'))
            );
            $_SESSION['LeadNewSource'] = $q;
        }
        
        header("Content-type: application/json; charset=utf-8");
        echo $collection->output();
        die();
    }
}