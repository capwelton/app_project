<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Project\Ui;

use Capwelton\App\Project\Set\LeadSet;
use Capwelton\App\Project\Set\Lead;

/**
 * @method \Func_App    App()
 */
class LeadModelView extends \app_TableModelView
{
    private $componentUi;
    protected $tagComponent;
    protected $tagSet;
    protected $leadCtrl;
    protected $leadComponent = null;

    /**
     * @param \Func_App $App
     * @param string $id
     */
    public function __construct(\Func_App $App, $id = null)
    {
        parent::__construct($App, $id);
        $this->componentUi = $App->Ui();

        $this->tagComponent = $App->getComponentByName('TAG');
        $this->tagSet = $App->TagSet();
        $this->leadCtrl = $App->Controller()->Lead();
        $this->leadComponent = $App->getComponentByName('Lead');
        
        $this->addClass('depends-Lead');
        $this->addClass("depends-{$App->classPrefix}Lead");
    }

    /**
     * @param \app_CtrlRecord $recordController
     * @return self
     */
    public function setRecordController(\app_CtrlRecord $recordController)
    {
        $this->recordController = $recordController;
        return $this;
    }

    /**
     * {@inheritDoc}
     * @see \app_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(LeadSet $LeadSet = null)
    {
        $App = $this->App();

        if (!isset($LeadSet)) {
            $LeadSet = $this->getRecordSet();
        }

        
        $this->addColumn(app_TableModelViewColumn('_actions_', ' ')->setVisible(true)->setSortable(false)->setExportable(false)->addClass('widget-column-thin'));
        $this->addColumn(
            app_TableModelViewColumn($LeadSet->title, $this->leadComponent->translate('Title'))
        );
        
        if($this->tagComponent){
            $this->addColumn(
                app_TableModelViewColumn('tag', $this->tagComponent->translate('Tag'))
            );
        }
        
        //ADD CUSTOM FIELDS
        $customFieldSet = $App->CustomFieldSet();
        $customFields = $customFieldSet->select(
            $customFieldSet->object->is('Lead')->_AND_(
                $customFieldSet->searchable->is(true)
            )
        );
        
        foreach ($customFields as $customField) {
            $fieldname = $customField->fieldname;
            $this->addColumn(
                app_TableModelViewColumn($LeadSet->$fieldname, $customField->name)
                ->setSortable(true)
                ->setExportable(true)
                ->setSearchable($customField->searchable)
            );
        }
    }

    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::initRow()
     */
    public function initRow(\ORM_Record $record, $row)
    {
        $this->addRowClass($row, 'widget-actions-target');
        return parent::initRow($record, $row);
    }

    /**
     *
     */
    protected function getDisplayAction(Lead $record)
    {
        return $this->leadCtrl->display($record->id);
    }

    /**
     * @param Lead    $record
     * @param string        $fieldPath
     * @return \Widget_Item
     */
    protected function computeCellContent(Lead $record, $fieldPath)
    {
        $App = $this->App();
        $W = bab_Widgets();

        switch ($fieldPath) {
            case 'title':
                $displayAction = $this->getDisplayAction($record);
                return $W->Link(
                    self::getRecordFieldValue($record, $fieldPath),
                    $displayAction
                );
            case '_actions_':
                $box = $W->HBoxItems();
                if ($record->isUpdatable()) {
                    $box->setSizePolicy(\Func_Icons::ICON_LEFT_SYMBOLIC);
                    $box->addItem(
                        $W->Link(
                            '',
                            $this->leadCtrl->setHighlightedRecords(array($record->id))
                        )->addClass('icon', \Func_Icons::ACTIONS_ARROW_RIGHT_DOUBLE, 'app_highlightButton')
                    ->setAjaxAction(null, '')
                    );
                }
                return $box;
        }

        return parent::computeCellContent($record, $fieldPath);
    }

    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::computeCellTextContent()
     */
    protected function computeCellTextContent(Lead $record, $fieldPath)
    {
        switch ($fieldPath) {
            case 'tag':
                if($this->tagSet){
                    $tags = $this->tagSet->selectFor($record);
                    $values = array();
                    foreach ($tags as $tag) {
                        $values[] = $tag->targetId->label;
                    }
                    return implode('; ', $values);
                }
                return '';
        }
        return parent::computeCellTextContent($record, $fieldPath);
    }
    
    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param array       $filter
     * @return \ORM_Criteria
     */
    public function getFilterCriteria($filter = null)
    {
        /* @var $recordSet LeadSet */
        $recordSet = $this->getRecordSet();
        $App = $this->App();
        $conditions = array(
            $recordSet->isReadable()
        );

        if (isset($filter['search']) && !empty($filter['search'])) {
            $mixedConditions = array(
                $recordSet->name->contains($filter['search']),
            );
            $conditions[] = $recordSet->any($mixedConditions);
        }        
        
        if (isset($filter['tag']) && !empty($filter['tag']) && $this->tagComponent) {
            $conditions[] = $recordSet->haveTagLabels($filter['tag']);
        }
        
        unset($filter['tag']);
        
        $conditions[] = parent::getFilterCriteria($filter);
        
        return $recordSet->all($conditions);
    }
}