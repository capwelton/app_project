<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Project\Ui;

use Capwelton\App\Project\Ctrl\ProjectController;

/**
 * OrganizationRecordView
 *
 * @method \Func_App    App()
 * @property Organization $record
 */
class ProjectRecordView extends \app_RecordView
{
    /**
     * @var ProjectController
     */
    protected $ctrlNoProxy;
    /**
     * @var ProjectController
     */
    protected $ctrl;
    
    public function __construct(\Func_App $App, $id = null, \Widget_Layout $layout = null)
    {
        parent::__construct($App, $id, $layout);
        $this->ctrlNoProxy = $App->Controller()->Project(false);
        $this->ctrl = $this->ctrlNoProxy->proxy();
    }
    /**
     * {@inheritDoc}
     * @see \app_UiObject::display()
     */
    public function display(\Widget_Canvas $canvas)
    {
        $this->addSections($this->getView());
        
        return parent::display($canvas);
    }
    
    protected function _address(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $address = $this->record->address();
        
        $box = $W->HBoxItems(
            $addressBox =  $W->VBoxItems()
        )->setSpacing(3, 'px');
        
        
        
        $addressComponent = $App->getComponentByName('ADDRESS');
        if (isset($address)) {
            $addressBox->addItems(
                $W->Label($address->street),
                $W->FlowItems(
                    $W->Label($address->postalCode),
                    $W->Label($address->city),
                    $W->Label($address->cityComplement)
                )->setHorizontalSpacing(1, 'ex'),
                $W->Label($address->country() ? $address->country()->getName() : '')
            );
        }
        
        return $this->labelledWidget(
            isset($label) ? $label : $addressComponent->translate('Address'),
            $box,
            $customSection
        );
    }
    
    protected function _attachments(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $attachmentComponent = $App->getComponentByName('ATTACHMENT');
        if(!isset($attachmentComponent)){
            return null;
        }
        
        return $this->labelledWidget(
            isset($label) ? $label : $attachmentComponent->translate('Attachments'),
            $App->Controller()->Attachment(false)->_attachments($this->record->getRef()),
            $customSection
        );
    }
    
    protected function _classifications(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        return $this->labelledWidget(
            $App->translate('Classifications'),
            $App->Ui()->ProjectClassificationPreview($this->record),
            $customSection
        );
    }
    
    protected function _tags(\app_CustomSection $customSection = null, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $tagComponent = $App->getComponentByName("TAG");
        if(!isset($tagComponent)){
            return null;
        }
        
        $tagsDisplay = $App->Ui()->TagsListForRecord($this->record, $App->Controller()->Project()->displayListForTag());
        $editor = new \app_Editor($App);
        $editor->addItem($App->Controller()->Tag(false)->SuggestTag()->setName('label'));
        
        $editor->setName('tag');
        $editor->setSaveAction($App->Controller()->Tag()->link(),$tagComponent->translate("Add"));
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setHiddenValue('to', $this->record->getRef());
        $editor->isAjax = true;
        
        return $this->labelledWidget(
            $tagComponent->translate('Tags'),
            $W->FlowItems(
                $tagsDisplay,
                $editor
            )->setHorizontalSpacing(0.5, 'em'),
            $customSection
        );
    }
    
    protected function _description(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Description'),
            $W->Html($this->record->description),
            $customSection
        );
    }
    
    protected function _number(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Number'),
            $W->Label($this->record->getFullNumber()),
            $customSection
        );
    }
    
    protected function _customer(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();
        
        $customer = $this->record->customer();
        
        $parentOrganizationLink = null;
        if ($customer) {
            $organizationLink = $W->Link($customer->name, $App->Controller()->Organization()->display($customer->id));
            $parentOrganization = $customer->parent();
            if ($parentOrganization) {
                $parentOrganizationLink = $W->Link(
                    '> ' . $parentOrganization->name,
                    $App->Controller()->Organization()->display($parentOrganization->id)
                )->addClass('widget-light');
            }
        } else {
            $organizationLink = $W->Label('-');
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Customer'),
            $W->FlowItems(
                $organizationLink,
                $parentOrganizationLink
            )->setHorizontalSpacing(1, 'em'),
            $customSection
        );
    }
    
    protected function _status(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Status'),
            $this->ctrlNoProxy->displayStatus($this->record->id),
            $customSection
        );
    }
    
    protected function _notes(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $section = $this->getSection($customSection->id);
        $contextMenu = $section->getContextMenu();
        $contextMenu->addItem(
            $W->Link(
                '',
                $App->Controller()->Note()->add($this->record->getRef())
            )->setTitle($App->translate('Add note'))->setIcon(\Func_Icons::ACTIONS_LIST_ADD)->addClass('section-button widget-actionbutton')->setOpenMode(\Widget_Link::OPEN_DIALOG)
        );
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Notes'),
            $App->Controller()->Note(false)->listFor($this->record->getRef()),
            $customSection
        );
    }
    
    protected function _teams(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $section = $this->getSection($customSection->id);
        $contextMenu = $section->getContextMenu();
        $contextMenu->addItem(
            $W->Link(
                '',
                $App->Controller()->Team()->add($this->record->getRef())
            )->setTitle($App->translate('Create a team'))->setIcon(\Func_Icons::ACTIONS_LIST_ADD)->addClass('section-button widget-actionbutton')->setOpenMode(\Widget_Link::OPEN_DIALOG)
        );
        
        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Teams'),
            $App->Controller()->Team(false)->listFor($this->record->getRef()),
            $customSection
        );
    }
    
    protected function _quotations($customSection)
    {
        return $this->ctrlNoProxy->quotationsTable($this->record->id);
    }

    protected function _responsible(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();

        $responsible = $this->record->responsible();
        $responsibleName = $responsible ? 
            $responsible->getFullName() :
            'This project has no designated responsible';

        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Project responsible'),
            $W->Label($responsibleName),
            $customSection
        );

    }

    protected function _startingDate(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $W = bab_Widgets();

        return $this->labelledWidget(
            !empty($label) ? $label : $App->translate('Starting date'),
            $W->Label($this->record->startingDate),
            $customSection
        );
    }
    
    
    
    protected function _photo(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        
        return $this->labelledWidget(
            isset($label) ? $label : $App->translate('Photo'),
            $App->Controller()->Project(false)->photo($this->record->id),
            $customSection
        );
    }
    
    protected function _pendingTasks(\app_CustomSection $customSection, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $taskC = $App->getComponentByName('Task');
        
        $section = $this->getSection($customSection->id);
        $contextMenu = $section->getContextMenu();
        if(!$contextMenu->getById($section->getId().'_addTaskLink')){
            $contextMenu->addItem(
                $W->Link(
                    '',
                    $App->Controller()->Task()->add($this->record->getRef()),
                    $section->getId().'_addTaskLink'
                )->addClass('section-button', 'widget-actionbutton', 'icon', \Func_Icons::ACTIONS_LIST_ADD)
                ->setSizePolicy('pull-up')
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
                ->setTitle($taskC->translate('Create a new task'))
            );
        }
        
        return $this->labelledWidget(
            !empty($label) ? $label : $taskC->translate('Pending tasks'),
            $App->Controller()->Task(false)->_pendingTasksFor($this->record->getRef()),
            $customSection
        );
    }
    
    protected function _completedTasks(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        
        $taskC = $App->getComponentByName('Task');
        
        return $this->labelledWidget(
            !empty($label) ? $label : $taskC->translate('Completed tasks'),
            $App->Controller()->Task(false)->_completedTasksFor($this->record->getRef()),
            $customSection
        );
    }
}
