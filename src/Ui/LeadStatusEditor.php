<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Project\Ui;

use Capwelton\App\LeadStatus\Set\LeadStatus;

/**
 * @return LeadStatusEditor
 */
class LeadStatusEditor extends \app_Editor
{
    protected $record;
    
    protected $leadComponent;
    
    /**
     * @param \Func_App App
     * @param LeadStatus $LeadStatus
     * @param int $id
     * @param \Widget_Layout $layout
     */
    public function __construct(\Func_App $App, LeadStatus $record = null, $id = null, \Widget_Layout $layout = null)
    {
        $this->record = $record;
        $this->leadComponent = $App->getComponentByName('Lead');
        parent::__construct($App, $id, $layout);
        $this->addFields();
    }

    protected function addFields()
    {
        $W = $this->widgets;
        
        $box = $W->VBoxItems()->setVerticalSpacing(1, 'em');
        
        $box->addItems(
            $this->name(),
            $this->id()
        )->setVerticalSpacing(1, 'em');
        
        $this->addItem($box);
    }
    
    protected function name()
    {
        $W = $this->widgets;
        
        return $W->LabelledWidget(
            $this->leadComponent->translate('Name'),
            $W->LineEdit(),
            'name'
        );
    }
    
    protected function id(){
        return $this->widgets->Hidden(null,'id',null);
    }
    
    
    public function setValues($LeadStatus, $namePathBase = array())
    {
        if ($LeadStatus instanceof LeadStatus) {
            $LeadStatusValues = $LeadStatus->getValues();
            if($LeadStatus->id){
                $this->disableInputs();
            }
            parent::setValues($LeadStatusValues, $namePathBase);
        } else {
            parent::setValues($LeadStatus, $namePathBase);
        }
    }
}