<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Project\Ui;


use Capwelton\App\Project\Set\Project;

class ProjectUi extends \app_Ui implements \app_ComponentUi
{    
    /**
     * Project editor
     * @return ProjectEditor
     */
    public function ProjectEditor(Project $record = null)
    {
        $editor = new ProjectEditor($this->app, $record);
        $editor->setHiddenValue('tg', bab_rp('tg'));
        
        return $editor;
    }
    
    /**
     * Project list
     * @return ProjectTableView
     */
    public function ProjectTableView()
    {
        return new ProjectTableView($this->app);
    }
    
    public function tableView()
    {
        return $this->ProjectTableView();
    }
    
    public function editor(Project $record = null)
    {
        return $this->ProjectEditor($record);
    }
    
    /**
     * @param Project $project
     * @return ProjectRecordView
     */
    public function ProjectFullFrame(Project $project)
    {
        $projectFullFrame = new ProjectRecordView($this->app);
        $projectFullFrame->setRecord($project);
        return $projectFullFrame;
    }    
    
    /**
     * Project section editor
     * @return ProjectSectionEditor
     */
    public function ProjectSectionEditor(Project $record = null)
    {
        return new ProjectSectionEditor($this->app, $record);
    }
    
    /**
     * Generates and returns an image representing of the specified project's logo.
     *
     * @param Project   $project    The project to generate a photo of.
     * @param int       $width      Width in px.
     * @param int       $height     Height in px.
     * @param bool      $border     True to add a 1px light-grey border around the image.
     
     * @return \Widget_Frame
     */
    public function ProjectPhoto(Project $project, $width, $height, $border = false)
    {
        $W = bab_Widgets();
        $T = @\bab_functionality::get('Thumbnailer');
        
        $image = $W->Image();
        if ($T) {
            $logo = $project->getPhotoPath($this->app);
            if (empty($logo)) {
                return $image;
            }
            
            $T->setSourceFile($logo->toString());
            $T->setResizeMode(\Func_Thumbnailer::KEEP_ASPECT_RATIO);
                
            
            if ($border) {
                $padWidth = min(array(2, round(min(array($width, $height)) / 24)));
                $T->setBorder(1, '#cccccc', $padWidth, '#ffffff');
            }
            $imageUrl = $T->getThumbnail($width, $height);
            $image->setUrl($imageUrl);
        }
        
        if ($project->name) {
            $image->setTitle($project->name);
        }
        
        $image->addClass('app-project-image app-element-image small');
        
        return $image;
    }
    
    /**
     *
     * @param Project $project
     * @return \Widget_FlowLayout
     */
    public function ProjectClassificationPreview(Project $project)
    {
        $App = $project->App();
        $W = bab_Widgets();
        
        $projectClassificationProjectSet = $App->ProjectClassificationProjectSet();
        $projectClassificationProjectSet->projectClassification();
        $projectClassifications = $projectClassificationProjectSet->selectForProject($project->id);
        
        $box = $W->FlowLayout();
        
        if($projectClassifications->count() == 0){
            $box->addItem(
                $W->Label($App->getComponentByName('Project')->translate('No classifications'))    
            );
            return $box;
        }
        
        foreach ($projectClassifications as $projectClassification) {
            $classificationPathname = $projectClassification->projectClassification->getPathname();
            
            $level = count($classificationPathname);
            
            $classificationLabel = $W->Label($projectClassification->projectClassification->name)
            ->setTitle(implode(' > ', $classificationPathname))
            ->addClass('app-classification app-classification-level-' . $level);
            
            $box->addItem($classificationLabel);
        }
        
        return $box;
    }
}
