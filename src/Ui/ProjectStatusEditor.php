<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Project\Ui;

class ProjectStatusEditor extends \app_Editor
{
    protected $projectComponent = null;
    
    public function __construct(\Func_App $app, $id = null, $layout = null)
    {
        parent::__construct($app, $id, $layout);
        $this->projectComponent = $app->getComponentByName('Project');
        $this->setHiddenValue('tg', $app->controllerTg);
    }
    
    protected function prependFields()
    {
        $this->name();
        $this->description();
        $this->icon();
        $this->color();
    }
    
    protected function name()
    {
        $W = bab_Widgets();
        $this->addItem(
            $W->LabelledWidget(
                $this->projectComponent->translate('Name'),
                $W->LineEdit()->setSize(60),
                'name'
            )
        );
    }

    protected function description()
    {
        $W = bab_Widgets();
        $this->addItem(
            $W->LabelledWidget(
                $this->projectComponent->translate('Description'),
                $W->TextEdit()->setLines(4)->addClass('widget-autoresize'),
                'description'
            )
        );
    }
    
    protected function icon()
    {
        $W = bab_Widgets();
        $radioMenu = $W->RadioMenu()
        ->width(100)
        ->menuWidth(680)
        ->addClass('icon-top-24 icon-top icon-24x24 widgets-radiomenu-multiple-items');
        
        
        $iconsFile = \bab_functionality::includefile('Icons');
        $iconReflector = new \ReflectionClass($iconsFile);
        $constants = array_flip($iconReflector->getConstants());
        \bab_Sort::natcasesort($constants);
        foreach($constants as $key => $constantName) {
            if (substr($constantName, 0, 5) === 'ICON_') {
                continue;
            }
            $radioMenu->addOption($key, $W->Icon(''/* $constantName */, eval('return '.$iconsFile.'::'.$constantName.';')));
        }
        
        
        $this->addItem(
            $W->LabelledWidget(
                $this->projectComponent->translate('Associated icon'),
                $radioMenu,
                'icon'
            )
        );
    }
    
    protected function color()
    {
        $W = bab_Widgets();
        
        $this->addItem(
            $W->LabelledWidget(
                $this->projectComponent->translate('Associated color'),
                $W->ColorPicker()
                ->setColorSpace('YUV')
                ->setRules('V', 'U', 'Y')
                ->setSliderValue(0.65),
                'color'
            )
        );
    }
}
