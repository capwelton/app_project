<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Project\Ui;

use Capwelton\App\Project\Set\Project;
use Capwelton\App\Project\Set\ProjectSet;
use Capwelton\App\Project\Set\ProjectClassificationProjectSet;
use Capwelton\App\Project\Set\ProjectClassificationProject;
use Capwelton\App\Project\Set\ProjectClassificationSet;

/**
 * @method \Func_App    App()
 * 
 * @property ProjectClassificationProjectSet $projectClassificationProjectSet
 */
class ProjectModelView extends \app_TableModelView
{
    protected $componentUi;
    protected $tagComponent;
    protected $tagSet;
    protected $projectCtrl;
    protected $organizationCtrl;
    protected $projectClassificationProjectSet;
    protected $projectComponent = null;

    /**
     * @param \Func_App $App
     * @param string $id
     */
    public function __construct(\Func_App $App, $id = null)
    {
        parent::__construct($App, $id);
        $this->componentUi = $App->Ui();

        $this->tagComponent = $App->getComponentByName('TAG');
        $this->tagSet = $App->TagSet();
        $this->projectCtrl = $App->Controller()->Project();
        $this->organizationCtrl = $App->Controller()->Organization();
        $this->projectClassificationProjectSet = $App->ProjectClassificationProjectSet();
        $this->projectClassificationProjectSet->projectClassification();
        $this->projectComponent = $App->getComponentByName('Project');
        
        $this->addClass('depends-Project');
        $this->addClass("depends-{$App->classPrefix}Project");
    }


    /**
     * @param \app_CtrlRecord $recordController
     * @return self
     */
    public function setRecordController(\app_CtrlRecord $recordController)
    {
        $this->recordController = $recordController;
        return $this;
    }

    /**
     * {@inheritDoc}
     * @see \app_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(ProjectSet $projectSet = null)
    {
        $App = $this->App();

        if (!isset($projectSet)) {
            $projectSet = $this->getRecordSet();
        }

        $projectSet->addFullNumberField();
        $projectSet->customer();
        $projectSet->address();
        $projectSet->status();

        
        $this->addColumn(app_TableModelViewColumn('_actions_', ' ')->setVisible(true)->setSortable(false)->setExportable(false)->addClass('widget-column-thin'));
        $this->addColumn(
            app_TableModelViewColumn('image', ' ')
                ->setSelectable(true, 'Image')
                ->setSortable(false)
                ->setExportable(false)
                ->addClass('widget-column-thin', 'widget-column-center')
        );
        $this->addColumn(
            app_TableModelViewColumn('imageSmall', ' ')
                ->setSelectable(true, 'Small image')
                ->setVisible(false)
                ->setSortable(false)
                ->setExportable(false)
                ->addClass('widget-column-thin', 'widget-column-center')
        );
        $this->addColumn(
            app_TableModelViewColumn('imageLarge', ' ')
                ->setSelectable(true, 'Large image')
                ->setVisible(false)
                ->setSortable(false)
                ->setExportable(false)
                ->addClass('widget-column-thin', 'widget-column-center')
        );
        $this->addColumn(
            app_TableModelViewColumn($projectSet->fullNumber, $this->projectComponent->translate('Number'))
                ->addClass('widget-column-thin')
        );
        $this->addColumn(
            app_TableModelViewColumn($projectSet->createdOn, $this->projectComponent->translate('Created on'))
            ->addClass('widget-column-thin')
        );
        $this->addColumn(
            app_TableModelViewColumn($projectSet->name, $this->projectComponent->translate('Name'))
        );
        $this->addColumn(
            app_TableModelViewColumn($projectSet->customer->name, $this->projectComponent->translate('Customer'))
        );
        $this->addColumn(
            app_TableModelViewColumn($projectSet->status, $this->projectComponent->translate('Status'))
        );
        $this->addColumn(
            app_TableModelViewColumn($projectSet->address->city, $this->projectComponent->translate('City'))
        );
        $this->addColumn(
            app_TableModelViewColumn($projectSet->address->postalCode, $this->projectComponent->translate('Postal code'))
        );
        $this->addColumn(
            app_TableModelViewColumn('classification', $this->projectComponent->translate('Classifications'))->setSearchable(true)
        );
        $this->addColumn(
            app_TableModelViewColumn($projectSet->responsible())->setVisible(false)
        );
        
        if($this->tagComponent){
            $this->addColumn(
                app_TableModelViewColumn('tag', $this->tagComponent->translate('Tag'))
            );
        }
        
        
        //ADD CUSTOM FIELDS
        $customFieldSet = $App->CustomFieldSet();
        $customFields = $customFieldSet->select(
            $customFieldSet->object->is('Project')->_AND_(
                $customFieldSet->searchable->is(true)
            )
        );
        
        foreach ($customFields as $customField) {
            $fieldname = $customField->fieldname;
            $this->addColumn(
                app_TableModelViewColumn($projectSet->$fieldname, $customField->name)
                ->setSortable(true)
                ->setExportable(true)
                ->setSearchable($customField->searchable)
            );
        }
    }


    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::initRow()
     */
    public function initRow(\ORM_Record $record, $row)
    {
        $this->addRowClass($row, 'widget-actions-target');
        return parent::initRow($record, $row);
    }


    /**
     *
     */
    protected function getDisplayAction(Project $record)
    {
        return $this->projectCtrl->display($record->id);
    }


    /**
     * @param Project    $record
     * @param string        $fieldPath
     * @return \Widget_Item
     */
    protected function computeCellContent(Project $record, $fieldPath)
    {
        $App = $this->App();
        $W = bab_Widgets();

        switch ($fieldPath) {
            case 'name':
            case 'fullNumber':
                $displayAction = $this->getDisplayAction($record);
                return $W->Link(
                    self::getRecordFieldValue($record, $fieldPath),
                    $displayAction
                );
            case 'customer/name':
                return $W->Link(
                    $record->customer->name,
                    $this->organizationCtrl->display($record->customer->id)
                );
            case 'classification':
                $projectClassifications = $this->projectClassificationProjectSet->selectForProject($record->id);
                $classifications = $W->FlowLayout()->setSpacing(3, 'px');
                foreach ($projectClassifications as $projectClassification) {
                    /* @var $projectClassification ProjectClassificationProject */
                    
                    $classificationLabel = $W->Label($projectClassification->projectClassification->name)
                    ->setTitle(implode(' > ', $projectClassification->projectClassification->getPathname()))
                    ->addClass('app-classification');
                    $classifications->addItem($classificationLabel);
                }
                return $classifications;
            case 'status':
                $status = $record->status();
                if (isset($status)) {
                    $statusName = $status->name;
                } else {
                    $statusName = '';
                }
                return $W->Label($statusName);
            case 'image':
                $displayAction = $this->getDisplayAction($record);
                return $W->Link(
                    $this->componentUi->ProjectPhoto($record, 40, 24),
                    $displayAction
                )->setSizePolicy('condensed');
            case 'imageSmall':
                $displayAction = $this->getDisplayAction($record);
                return $W->Link(
                    $this->componentUi->ProjectPhoto($record, 24, 24),
                    $displayAction
                )->setSizePolicy('condensed');
            case 'imageLarge':
                $displayAction = $this->getDisplayAction($record);
                return $W->Link(
                    $this->componentUi->ProjectPhoto($record, 80, 64),
                    $displayAction
                )->setSizePolicy('condensed');
            case '_actions_':
                $box = $W->HBoxItems();
                if ($record->isUpdatable()) {
                    $box->setSizePolicy(\Func_Icons::ICON_LEFT_SYMBOLIC);
                    $box->addItem(
                        $W->Link(
                            '',
                            $this->projectCtrl->setHighlightedRecords(array($record->id))
                        )->addClass('icon', \Func_Icons::ACTIONS_ARROW_RIGHT_DOUBLE, 'app_highlightButton')
                    ->setAjaxAction(null, '')
                    );
                }
                return $box;
        }

        return parent::computeCellContent($record, $fieldPath);
    }

    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::computeCellTextContent()
     */
    protected function computeCellTextContent(Project $record, $fieldPath)
    {
        switch ($fieldPath) {
            case 'responsible':
                return $record->responsible->getFullName();  
            case 'tag':
                if($this->tagSet){
                    $tags = $this->tagSet->selectFor($record);
                    $values = array();
                    foreach ($tags as $tag) {
                        $values[] = $tag->targetId->label;
                    }
                    return implode('; ', $values);
                }
                return '';
        }
        return parent::computeCellTextContent($record, $fieldPath);
    }
    
    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param array       $filter
     * @return \ORM_Criteria
     */
    public function getFilterCriteria($filter = null)
    {
        /* @var $recordSet ProjectSet */
        $recordSet = $this->getRecordSet();
        $App = $this->App();
        $conditions = array(
            $recordSet->isReadable()
        );

        if (isset($filter['search']) && !empty($filter['search'])) {
            $mixedConditions = array(
                $recordSet->name->contains($filter['search']),
            );
            $conditions[] = $recordSet->any($mixedConditions);
        }
        
        if (isset($filter['status']) && !empty($filter['status'])) {
            $conditions[] = $recordSet->status()->id->in($filter['status']);
        }
        
        if (isset($filter['tag']) && !empty($filter['tag']) && $this->tagComponent) {
            $conditions[] = $recordSet->haveTagLabels($filter['tag']);
        }
        
        if (isset($filter['fullNumber']) && !empty($filter['fullNumber'])) {
            $fullNumbers = explode(',', $filter['fullNumber']);
            $fullNumberConditions = array();
            foreach ($fullNumbers as $fullNumber) {
                $fullNumberConditions[] = $recordSet->fullNumber->contains($fullNumber);
            }
            $conditions[] = $recordSet->any($fullNumberConditions);
        }
        
        if (isset($filter['classification']) && !empty($filter['classification'])) {            
            $projectClassificationSet = $this->projectClassificationProjectSet;
            $conditions[] = $recordSet->id->in(
                $projectClassificationSet->projectClassification->in($filter['classification']),
                'project'
            );
        }
        
        unset($filter['status']);
        unset($filter['tag']);
        unset($filter['fullNumber']);
        unset($filter['classification']);
        
        $conditions[] = parent::getFilterCriteria($filter);
        
        return $recordSet->all($conditions);
    }
    
   
     /**
     * Handle filter field input widget
     * If the method returns null, no filter field is displayed for the field
     *
     * @param   string          $name       table field name
     * @param   \ORM_Field       $field      ORM field
     *
     * @return \Widget_InputWidget | null
     */
    protected function handleFilterInputWidget($name, \ORM_Field $field = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        switch ($name){
            case 'status':
                $select = $W->Select2()->setMultiple();
                $statusSet = $App->ProjectStatusSet();
                $statuses = $statusSet->select(
                    $statusSet->parent->isNot(0)    
                )->orderAsc($statusSet->name);
                $options = array();
                foreach ($statuses as $status){
                    $options[$status->id] = $status->name;
                }
                $select->setOptions($options);
                return $select;
            case 'classification':
                $select = $W->Select2()->setMultiple();
                $classificationSet = $App->ProjectClassificationSet();
                $classifications = $classificationSet->select(
                    $classificationSet->parent->isNot(0)    
                )->orderAsc($classificationSet->name);
                $options = array();
                foreach ($classifications as $classification){
                    $options[$classification->id] = $classification->name;
                }
                $select->setOptions($options);
                return $select;
        }

        return parent::handleFilterInputWidget($name, $field);
    }
}