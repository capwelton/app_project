<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Project\Ui;

use Capwelton\App\Project\Set\Project;

/**
 *
 * @method \Func_App App()
 */
class ProjectSectionEditor extends \app_RecordEditor
{
    
    protected $parentOrganizationItem = null;
    
    protected $parentOrganizationField = null;
    
    public function setRecord($record)
    {
        $this->record = $record;
        $this->recordSet = $record->getParentSet();
        
        if ($name = $this->getName()) {
            $values = $record->getFormOutputValues();
            $values['number'] = $record->getFullNumber();
            
            
            $this->setValues($values, array($name));
        }
        
        return $this;
    }
    
    /**
     * {@inheritDoc}
     * @see \app_Editor::setValues()
     */
    public function setValues($project, $namePathBase = array())
    {
        $App = $this->App();
        
        if ($project instanceof Project) {
            
            $values = $project->getFormOutputValues();
            $values['number'] = $project->getFullNumber();
            
            $this->setValues(array('data' => $values));
            
            $projectClassificationProjectSet = $App->ProjectClassificationProjectSet();
            $projectClassifications = $projectClassificationProjectSet->select(
                $projectClassificationProjectSet->project->is($project->id)
            );
            $classificationValues = array();
            foreach ($projectClassifications as $projectClassification) {
                $classificationValues[] = $projectClassification->projectClassification;
            }
            
            $this->setValue(array('data', 'types'), $classificationValues);
        } else {
            parent::setValues($project, $namePathBase);
        }
        return $this;
    }
    
    protected function _createdBy(\app_CustomSection $customSection)
    {
        return null;
    }
    
    protected function _createdOn(\app_CustomSection $customSection)
    {
        return null;
    }
    
    protected function _modifiedBy(\app_CustomSection $customSection)
    {
        return null;
    }
    
    protected function _modifiedOn(\app_CustomSection $customSection)
    {
        return null;
    }
    
    protected function _id(\app_CustomSection $customSection)
    {
        return null;
    }
    
    protected function _uuid(\app_CustomSection $customSection)
    {
        return null;
    }
    
    protected function _address(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $addressComponent = $App->getComponentByName('ADDRESS');
        $W = $this->widgets;
        $box = $W->NamedContainer('address')->setSizePolicy('widget-100pc');
        $box->addItem(
            $App->Ui()->AddressEditor()->showMapPreview(false)->setSizePolicy('widget-100pc')
        );
        return $this->labelledField(
            !empty($label) ? $label : $addressComponent->translate('Address'),
            $box
        );
    }
    
    protected function _classifications(\app_CustomSection $customSection)
    {
        return null;
    }
    
    protected function _status(\app_CustomSection $customSection)
    {
        return null;
    }
    
    protected function _customer(\app_CustomSection $customSection, $label = null)
    {
        return null;
    }
    
    protected function _description(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        $W = $this->widgets;
        return $this->labelledField(
            !empty($label) ? $label : $appC->translate('Description'),
            $this->descriptionItem = $W->CKEditor()
            ->addClass('widget-100pc'),
            'description'
        );
    }
    
    protected function _number(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        $W = $this->widgets;
        return $this->labelledField(
            !empty($label) ? $label : $appC->translate('Number'),
            $W->LineEdit()->disable(),
            'number'
        );
    }
    
    protected function _responsible(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $appC = $App->getComponentByName('Project');
        
        $suggest = $App->Ui()->SuggestContact();
        
        if($responsible = $this->record->responsible()){
            $suggest->addOption($responsible->id, $responsible->getFullName());
            $suggest->setValue($responsible->id);
        }
        else{
            $responsibleId = 0;
            $responsibleName = '';
            
            $currentContact = $App->ContactSet()->getCurrentContact();
            if($currentContact){
                $responsibleId = $currentContact->id;
                $responsibleName = $currentContact->getFullName();
            }
            
            $suggest->addOption($responsibleId, $responsibleName);
            $suggest->setValue($responsibleId);
        }
        
        return $this->labelledField(
            !empty($label) ? $label : $appC->translate('Responsible'),
            $suggest,
            'responsible'
        );
    }
}