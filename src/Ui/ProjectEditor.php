<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Project\Ui;

use Capwelton\App\Project\Set\Project;

/**
 * Project editor
 *
 * @method \Func_App    App()
 */
class ProjectEditor extends \app_Editor
{
    
    protected $nameItem = null;
    
    /**
     * @var Project
     */
    protected $project;
    
    protected $projectComponent = null;
    
    /**
     * @param \Widget_Layout $layout	The layout that will manage how widgets are displayed in this form.
     * @param string $id			The item unique id.
     */
    public function __construct(\Func_App $App, Project $project = null, $id = null, \Widget_Layout $layout = null)
    {
        if (null === $layout) {
            $W = bab_Widgets();
            $layout = $W->VBoxLayout()->setVerticalSpacing(2, 'em');
        }
        
        parent::__construct($App, $id, $layout);
        $this->project = $project;
        $this->projectComponent = $App->getComponentByName('Project');
        
        $this->colon();
        $this->setHiddenValue('tg', $App->controllerTg);
        $this->setName('data');
        
        $this->addButtons();
        
        if (isset($project)) {
            $this->setValues($project->getValues(), array('data'));
        }
    }
    
    /**
     * @param	array	$row
     * @param	array 	$namePathBase
     * @return 	ProjectEditor
     */
    public function setValues($project, $namePathBase = array())
    {
        if (!($project instanceof Project)) {
            return parent::setValues($project, $namePathBase);
        }
        
        $projectValues = $project->getValues();
        parent::setValues($projectValues, $namePathBase);
        return $this;
    }
    
    
    /**
     * Add a default field set to form
     */
    protected function appendFields()
    {
        $W = bab_Widgets();
        $this->addItem($this->id());
        $this->addItem(
            $W->VBoxItems(
                $this->name(),
                $this->description(),
                $this->customer()
            )    
        );
    }
    
    protected function addButtons()
    {
        $App = $this->App();
        $this->setSaveAction($App->Controller()->Project()->save());
    }
    
    
    protected function id()
    {
        $W = $this->widgets;
        return $W->Hidden()->setName('id');
    }
    
    protected function name()
    {
        $W = $this->widgets;
        return $this->labelledField(
            $this->projectComponent->translate('Name'),
            $this->nameItem = $W->LineEdit()
            ->addClass('widget-100pc')
            ->setMandatory(true, $this->projectComponent->translate('You must specify the project name')),
            'name'
        );
    }
    
    protected function description()
    {
        $W = $this->widgets;
        return $this->labelledField(
            $this->projectComponent->translate('Description'),
            $this->descriptionItem = $W->CKEditor()
            ->addClass('widget-100pc'),
            'description'
        );
    }
    
    protected function customer()
    {
        $App = $this->App();
        return $this->labelledField(
            $this->projectComponent->translate('Customer'),
            $App->Ui()->SuggestOrganization()
            ->addClass('widget-100pc'),
            'customer'
        );
    }
}
