<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Project\Ui;

use Capwelton\App\Project\Set\Lead;

/**
 * Project editor
 *
 * @method \Func_App    App()
 */
class LeadEditor extends \app_Editor
{
    
    protected $nameItem = null;
    
    /**
     * @var Lead
     */
    protected $lead;
    
    protected $leadComponent = null;
    
    /**
     * @param \Widget_Layout $layout	The layout that will manage how widgets are displayed in this form.
     * @param string $id			The item unique id.
     */
    public function __construct(\Func_App $App, Lead $lead = null, $id = null, \Widget_Layout $layout = null)
    {
        if (null === $layout) {
            $W = bab_Widgets();
            $layout = $W->VBoxLayout()->setVerticalSpacing(2, 'em');
        }
        
        parent::__construct($App, $id, $layout);
        $this->lead = $lead;
        $this->leadComponent = $App->getComponentByName('Lead');
        
        $this->colon();
        $this->setHiddenValue('tg', $App->controllerTg);
        $this->setName('data');
        
        $this->addButtons();
        
        if (isset($lead)) {
            $this->setValues($lead->getValues(), array('data'));
        }
    }
    
    /**
     * @param	array	$row
     * @param	array 	$namePathBase
     * @return 	LeadEditor
     */
    public function setValues($lead, $namePathBase = array())
    {
        if (!($lead instanceof Lead)) {
            return parent::setValues($lead, $namePathBase);
        }
        
        $leadValues = $lead->getValues();
        parent::setValues($leadValues, $namePathBase);
        return $this;
    }
    
    
    /**
     * Add a default field set to form
     */
    protected function appendFields()
    {
        $W = bab_Widgets();
        $this->addItem($this->id());
        $this->addItem(
            $W->VBoxItems(
                $W->FlexItems(
                    $this->type(),
                    $this->source(),
                    $this->status()
                )->setGrowable(),
                $W->FlexItems(
                    $this->title(),
                    $this->date()
                )->setGrowable(),
                $this->origin(),
                $this->content()
            )    
        );
    }
    
    protected function type()
    {
        $App = $this->App();
        $W = $this->widgets;
        $set = $App->LeadTypeSet();
        $records = $set->select()->orderAsc($set->name);
        $options = array();
        foreach ($records as $record){
            $options[$record->id] = $record->name;
        }
        
        return $W->LabelledWidget(
            $this->leadComponent->translate('Type'),
            $W->Select2()->setOptions($options),
            'type'
        );
    }
    
    protected function source()
    {
        $App = $this->App();
        $W = $this->widgets;
        
        $select = $W->Select2();
        $select->setDataSource($App->Controller()->LeadSource()->search());
        if(isset($this->record) && $source = $this->record->source()){
            $select->addOption($source->id, $source->name);
        }
        
        return $W->LabelledWidget(
            $this->leadComponent->translate('Source'),
            $select,
            'source'
        );
    }
    
    protected function status()
    {
        $App = $this->App();
        $W = $this->widgets;
        $set = $App->LeadStatusSet();
        $records = $set->select();
        $options = array();
        foreach ($records as $record){
            $options[$record->id] = $record->name;
        }
        
        return $W->LabelledWidget(
            $this->leadComponent->translate('Status'),
            $W->Select2()->setOptions($options),
            'status'
        );
    }
    
    protected function title()
    {
        $W = $this->widgets;
        
        return $W->LabelledWidget(
            $this->leadComponent->translate('Title'),
            $W->LineEdit(),
            'title'
        );
    }
    
    protected function date()
    {
        $W = $this->widgets;
        
        $picker = $W->DatePicker();
        if(!isset($this->record) || empty($this->record->date) || $this->record->date == '0000-00-00'){
            $picker->setValue(date('Y-m-d'));
        }
        
        return $W->LabelledWidget(
            $this->leadComponent->translate('Date'),
            $picker,
            'date'
        );
    }
    
    protected function origin()
    {
        $W = $this->widgets;
        
        return $W->LabelledWidget(
            $this->leadComponent->translate('Origin'),
            $W->LineEdit(),
            'origin'
        );
    }
    
    protected function content()
    {
        $W = $this->widgets;
        
        return $W->LabelledWidget(
            $this->leadComponent->translate('Content'),
            $W->TextEdit()->setLines(10)->addAttribute('height', 'auto'),
            'content'
        );
    }
    
    protected function addButtons()
    {
        $App = $this->App();
        $this->setSaveAction($App->Controller()->Lead()->save());
    }
    
    
    protected function id()
    {
        $W = $this->widgets;
        return $W->Hidden()->setName('id');
    }
}
