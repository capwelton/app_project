<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Project\Ui;

use Capwelton\App\Project\Ctrl\LeadController;
use Capwelton\App\Project\Set\Lead;

/**
 * OrganizationRecordView
 *
 * @method \Func_App    App()
 * @property Organization $record
 */
class LeadRecordView extends \app_RecordView
{
    /**
     * @var LeadController
     */
    protected $ctrlNoProxy;
    /**
     * @var LeadController
     */
    protected $ctrl;
    
    protected $leadComponent = null;
    
    public function __construct(\Func_App $App, $id = null, \Widget_Layout $layout = null)
    {
        parent::__construct($App, $id, $layout);
        $this->ctrlNoProxy = $App->Controller()->Lead(false);
        $this->ctrl = $this->ctrlNoProxy->proxy();
        $this->leadComponent = $App->getComponentByName('Lead');
    }
    /**
     * {@inheritDoc}
     * @see \app_UiObject::display()
     */
    public function display(\Widget_Canvas $canvas)
    {
        $this->addSections($this->getView());
        
        return parent::display($canvas);
    }
    
    protected function _attachments(\app_CustomSection $customSection, $label = null)
    {
        $App = $this->App();
        $attachmentComponent = $App->getComponentByName('ATTACHMENT');
        if(!isset($attachmentComponent)){
            return null;
        }
        
        return $this->labelledWidget(
            isset($label) ? $label : $attachmentComponent->translate('Attachments'),
            $App->Controller()->Attachment(false)->_attachments($this->record->getRef()),
            $customSection
        );
    }
    
    protected function _tags(\app_CustomSection $customSection = null, $label = null)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        $tagComponent = $App->getComponentByName("TAG");
        if(!isset($tagComponent)){
            return null;
        }
        
        $tagsDisplay = $App->Ui()->TagsListForRecord($this->record, $App->Controller()->Lead()->displayListForTag());
        $editor = new \app_Editor($App);
        $editor->addItem($App->Controller()->Tag(false)->SuggestTag()->setName('label'));
        
        $editor->setName('tag');
        $editor->setSaveAction($App->Controller()->Tag()->link(),$tagComponent->translate("Add"));
        $editor->setHiddenValue('tg', $App->controllerTg);
        $editor->setHiddenValue('to', $this->record->getRef());
        $editor->isAjax = true;
        
        return $this->labelledWidget(
            $tagComponent->translate('Tags'),
            $W->FlowItems(
                $tagsDisplay,
                $editor
            )->setHorizontalSpacing(0.5, 'em'),
            $customSection
        );
    }
    
    protected function _notes($customSection)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        return $this->labelledWidget(
            $this->leadComponent->translate('Notes'),
            $W->VBoxItems(
                $W->Link(
                    '',
                    $App->Controller()->Note()->add($this->record->getRef())
                )->addClass('section-button', 'widget-actionbutton', 'icon', \Func_Icons::ACTIONS_LIST_ADD)
                ->setSizePolicy('pull-up')
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
                ->setTitle($this->leadComponent->translate('Add note')),
                $App->Controller()->Note(false)->listFor($this->record->getRef())
            )->setIconFormat(16, 'left'),
            $customSection
        );
    }
    
    protected function _teams($customSection)
    {
        $W = bab_Widgets();
        $App = $this->App();
        
        return $this->labelledWidget(
            $this->leadComponent->translate('Teams'),
            $W->VBoxItems(
                $W->Link(
                    '',
                    $App->Controller()->Team()->add($this->record->getRef())
                )->addClass('section-button', 'widget-actionbutton', 'icon', \Func_Icons::ACTIONS_LIST_ADD)
                ->setSizePolicy('pull-up')
                ->setOpenMode(\Widget_Link::OPEN_DIALOG)
                ->setTitle($this->leadComponent->translate('Add team')),
                $App->Controller()->Team(false)->listFor($this->record->getRef())
            )->setIconFormat(16, 'left'),
            $customSection
        );
    }
}
