<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Project\Ui;


use Capwelton\App\Project\Set\Project;
use Capwelton\App\Project\Set\Lead;

class LeadUi extends \app_Ui implements \app_ComponentUi
{    
    /**
     * Lead editor
     * @return LeadEditor
     */
    public function LeadEditor(Lead $record = null)
    {
        $editor = new LeadEditor($this->app, $record);
        $editor->setHiddenValue('tg', bab_rp('tg'));
        
        return $editor;
    }
    
    /**
     * Lead list
     * @return LeadTableView
     */
    public function LeadTableView()
    {
        return new LeadTableView($this->app);
    }
    
    public function tableView()
    {
        return $this->LeadTableView();
    }
    
    public function editor(Lead $record = null)
    {
        return $this->LeadEditor($record);
    }
    
    /**
     * @param Lead $Lead
     * @return LeadRecordView
     */
    public function LeadFullFrame(Lead $Lead)
    {
        $LeadFullFrame = new LeadRecordView($this->app);
        $LeadFullFrame->setRecord($Lead);
        return $LeadFullFrame;
    }    
    
    /**
     * Lead section editor
     * @return LeadSectionEditor
     */
    public function LeadSectionEditor(Lead $record = null)
    {
        return new LeadSectionEditor($this->app, $record);
    }
}
