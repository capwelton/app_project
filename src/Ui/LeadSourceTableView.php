<?php
// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */

namespace Capwelton\App\Project\Ui;

use Capwelton\App\Project\Ctrl\LeadSourceController;
use Capwelton\App\Project\Set\LeadSource;
use Capwelton\App\Project\Set\LeadSourceSet;

class LeadSourceTableView extends \app_TableModelView
{
    protected $widgets;
    
    /**
     * @param \Func_App $App
     * @param string $id
     */
    public function __construct(\Func_App $App = null, $id = null)
    {
        $this->recordController = $App->Controller()->LeadSource();
        $this->widgets = bab_Widgets();
        parent::__construct($App, $id);
        $this->addClass(\Func_Icons::ICON_LEFT_16);
        $this->addClass('depends-LeadSource');
    }
    
    
    /**
     * @param \app_CtrlRecord $recordController
     * @return self
     */
    public function setRecordController(LeadSourceController $recordController)
    {
        $this->recordController = $recordController;
        return $this;
    }
    
    
    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::addDefaultColumns()
     */
    public function addDefaultColumns(\ORM_RecordSet $set)
    {
        /* @var $set LeadSourceSet */
        $this->addColumn(app_TableModelViewColumn($set->name));
    }
    
    
    /**
     * {@inheritDoc}
     * @see \widget_TableModelView::computeCellContent()
     */
    protected function computeCellContent(LeadSource $record, $fieldPath)
    {
        $W = $this->widgets;        
        $App = $this->App();
        
        switch ($fieldPath) {
            case 'name':
                $content = $W->VBoxItems();
                $content->setVerticalSpacing(1,"em");
                
                $content->addItem(
                    $W->Link(
                        $record->name,
                        $this->getRecordControllerProxy()->edit($record->id)
                    )->setOpenMode(\Widget_Link::OPEN_DIALOG_AND_RELOAD)
                    ->setIcon(\Func_Icons::ACTIONS_DOCUMENT_EDIT)
                );
                break;
            default:
                $content = parent::computeCellContent($record, $fieldPath);
                break;
        }
        
        return $content;
    }
    
    
    /**
     * Returns the criteria on the specified Set corresponding
     * to the filter array.
     *
     * @param array       $filter
     * @return \ORM_Criteria
     */
    public function getFilterCriteria($filter = null)
    {
        $recordSet = $this->getRecordSet();
        
        $conditions = array(
            $recordSet->isReadable()
        );
        
        $conditions[] = parent::getFilterCriteria($filter);
        
        if (isset($filter['search']) && !empty($filter['search'])) {
            $mixedConditions = array(
                $recordSet->name->contains($filter['search']),
            );
            $conditions[] = $recordSet->any($mixedConditions);
        }
        
        
        return $recordSet->all($conditions);
    }
}