<?php

// -------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
// -------------------------------------------------------------------------
/**
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2022 by SI4YOU ({@link https://www.siforyou.com/})
 */


namespace Capwelton\App\Project\ComponentDefinition;

use Capwelton\App\Project\Ctrl\ProjectClassificationController;
use Capwelton\App\Project\Ctrl\ProjectController;
use Capwelton\App\Project\Set\ProjectSet;
use Capwelton\App\Project\Set\ProjectStatusSet;
use Capwelton\App\Project\Set\ProjectStatusHistorySet;
use Capwelton\App\Project\Set\ProjectCounterSet;
use Capwelton\App\Project\Ctrl\ProjectStatusController;
use Capwelton\App\Project\Ui\ProjectClassificationUi;
use Capwelton\App\Project\Ui\ProjectStatusUi;
use Capwelton\App\Project\Ui\ProjectUi;
use Capwelton\App\Project\Set\ProjectClassificationSet;
use Capwelton\App\Project\Set\ProjectClassificationProjectSet;
use Capwelton\App\Project\Set\LeadSet;
use Capwelton\App\Project\Ui\LeadUi;
use Capwelton\App\Project\Ctrl\LeadController;
use Capwelton\App\Project\Set\LeadTypeSet;
use Capwelton\App\Project\Set\LeadStatusSet;
use Capwelton\App\Project\Ctrl\LeadTypeController;
use Capwelton\App\Project\Ui\LeadTypeUi;
use Capwelton\App\Project\Ctrl\LeadStatusController;
use Capwelton\App\Project\Ui\LeadStatusUi;
use Capwelton\App\Project\Set\LeadSourceSet;
use Capwelton\App\Project\Ctrl\LeadSourceController;
use Capwelton\App\Project\Ui\LeadSourceUi;

class ComponentDefinition implements \app_ComponentDefinition
{   
    public function getDefinition()
    {
        return 'Manages Projects';
    }
    
    public function getComponents(\Func_App $App)
    {
        $ProjectComponent = $App->createComponent(ProjectSet::class, ProjectController::class, ProjectUi::class);
        $ProjectStatusComponent = $App->createComponent(ProjectStatusSet::class, ProjectStatusController::class, ProjectStatusUi::class);
        $ProjectStatusHistoryComponent = $App->createComponent(ProjectStatusHistorySet::class, null, null);
        $ProjectCounterComponent = $App->createComponent(ProjectCounterSet::class, null, null);
        $ProjectClassificationComponent = $App->createComponent(ProjectClassificationSet::class, ProjectClassificationController::class, ProjectClassificationUi::class);
        $ProjectClassificationProjectComponent = $App->createComponent(ProjectClassificationProjectSet::class, ProjectClassificationController::class, ProjectClassificationUi::class);
        $LeadComponent = $App->createComponent(LeadSet::class, LeadController::class, LeadUi::class);
        $LeadTypeComponent = $App->createComponent(LeadTypeSet::class, LeadTypeController::class, LeadTypeUi::class);
        $LeadStatusComponent = $App->createComponent(LeadStatusSet::class, LeadStatusController::class, LeadStatusUi::class);
        $LeadSourceComponent = $App->createComponent(LeadSourceSet::class, LeadSourceController::class, LeadSourceUi::class);
        
        return array(
            'PROJECT' => $ProjectComponent,
            'PROJECTSTATUS' => $ProjectStatusComponent,
            'PROJECTSTATUSHISTORY' => $ProjectStatusHistoryComponent,
            'PROJECTCOUNTER' => $ProjectCounterComponent,
            'PROJECTCLASSIFICATION' => $ProjectClassificationComponent,
            'PROJECTCLASSIFICATIONPROJECT' => $ProjectClassificationProjectComponent,
            'LEAD' => $LeadComponent,
            'LEADTYPE' => $LeadTypeComponent,
            'LEADSTATUS' => $LeadStatusComponent,
            'LEADSOURCE' => $LeadSourceComponent
        );
    }

    public function getLangPath(\Func_App $App)
    {
        $addon = $App->getAddon();
        if(!$addon){
            return null;
        }
        return $addon->getPhpPath().'vendor/capwelton/appproject/src/langfiles/';
    }
    
    public function getStylePath(\Func_App $App)
    {
        return null;
    }
    
    public function getScriptPath(\Func_App $App)
    {
        return null;
    }
    
    
    
    public function getConfiguration(\Func_App $App)
    {
        $W = bab_Widgets();
        $component = $App->getComponentByName('Project');
        return array(
            array(
                'sectionName' => $component->translate('Project'),
                'sectionContent' => array(
                    $W->Link(
                        $W->Icon($component->translate('Project statuses'), \Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                        $App->Controller()->ProjectStatus()->displayList()
                    ),
                    $W->Link(
                        $W->Icon($component->translate('Project classifications'), \Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                        $App->Controller()->ProjectClassification()->displayList()
                    )
                )
            ),
            array(
                'sectionName' => $component->translate('Lead'),
                'sectionContent' => array(
                    $W->Link(
                            $W->Icon($component->translate('Lead types'), \Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                            $App->Controller()->LeadType()->displayList()
                    ),
                    $W->Link(
                            $W->Icon($component->translate('Lead statuses'), \Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                            $App->Controller()->LeadStatus()->displayList()
                    ),
                    $W->Link(
                            $W->Icon($component->translate('Lead sources'), \Func_Icons::ACTIONS_VIEW_LIST_DETAILS),
                            $App->Controller()->LeadSource()->displayList()
                    )
                )
            )
        );
    }
}